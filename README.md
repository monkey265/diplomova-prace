# Úvod

 
  Všechny asynchronní entity se jmenují jako jejich synchrnonní protějšky, ale mají prefix async_
  Vstupy/výstupy do/z bloku mají suffix _i/_o

## Links
>TODO seřadit
- [Modelsim reference manual](https://www.microsemi.com/document-portal/doc_view/131617-modelsim-reference-manual)
- [Design Vision user guide](https://picture.iczhiku.com/resource/eetop/sYkeEthQgUUeZnNX.pdf)
- [VSG - VHDL Style guide docs](https://vhdl-style-guide.readthedocs.io/en/latest/)
- [VHDL Low carb guide](https://dcenet.fel.cvut.cz/edu/fpga/doc/loCarb_VHDL_small.pdf)
- [Design Vision tutorial](https://ati.ttu.ee/IAY0340_2014/labs/lab12/design_vision_guide/design_vision_guide.html)
- [VHDL library introduction](https://www.arl.wustl.edu/projects/fpx/class/resources/Libraries%20and%20Packages%20in%20VHDL.htm)
- [VHDL Formatter](https://github.com/g2384/VHDLFormatter)
- [Design and FPGA-implementation of Asynchronous Circuits Using Two-Phase Handshaking](https://ieeexplore.ieee.org/document/8850673)
- [Click Elements: An Implementation Style for Data-Driven Compilation](https://ieeexplore.ieee.org/document/5476997)
- [ASYNC 2022 Summer School](https://asyncsymposium.org/async2022/)
- [The ACT VLSI Design Tools](https://avlsi.csl.yale.edu/act/doku.php)
- [Towards a Complete Methodology for Synthesizing Bundled-Data Asynchronous Circuits on FPGAs](https://www.researchgate.net/publication/335648750_Towards_a_Complete_Methodology_for_Synthesizing_Bundled-Data_Asynchronous_Circuits_on_FPGAs)
- [The VLSI-programming language Tangram and its translation into handshake circuits](https://ieeexplore.ieee.org/document/206431)
- [Introduction to Silicon Programming in the Tangram/Haste language](https://www.slideserve.com/lawrence-miles/introduction-to-silicon-programming-in-the-tangram-haste-language)
- [Behavioral Synthesis of Asynchronous Circuits Using Syntax Directed Translation as Backend](https://backend.orbit.dtu.dk/ws/portalfiles/portal/3459606/Behavioral%20Synthesis%20Nielsen%20et%20al.pdf)
- [The Balsa Asynchronous Circuit Synthesis System](https://apt.cs.manchester.ac.uk/ftp/pub/apt/papers/FDL00.pdf)

# TODO

- source/design_pkg.vhd:36:   --TODO make synthesis friendly version
- source/RC4/RC4_KSA.vhd:81:          array_position <= (counter_int mod 64); --TODO - maybe merge it into S_PERMUTE_S
- source/TEA_encrypt.vhd:60:    CASE(state) IS --TODO - Fix this, all inputs should be here
- source/RC4/RC4_KSA.vhd:81:          array_position <= (counter_int mod 64); --TODO - maybe merge it into S_PERMUTE_S
- fpga/sources/uart_controller.vhd:145:					--TODO - flash a led
- fpga/tb/uart_controller3_tb.vhd:49:       o_rx_dv     => open, -- TODO implement this to the controller
