LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

    use work.design_pkg.all;

entity flags_register is
    Port ( 
        -- Inputs
        clk_in             : IN STD_LOGIC;  -- Clock input
        rst_in             : IN STD_LOGIC;  -- Reset signal
        FSM_status_in      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        encr_requested_in  : IN STD_LOGIC;  -- Signal from UART Controller, 
        -- Outputs
        encr_requested_flg : OUT STD_LOGIC;  -- Encryption was requested via UART
        FSM_status_flg     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  -- Current status of encryption/decryption FSM


