--
--
-- Desc: Ovl�d� UART komunikaci, cte a zapisuje do registru dle prichozi UART
-- komunikace.

LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

library main_lib;
    use main_lib.tb_pkg.all;

library fpga_lib;

	ENTITY uart_controller
	IS
		PORT (
			-- Inputs
			clk_in             : IN STD_LOGIC; 
			rst_in             : IN STD_LOGIC; 
			-- FSM_status_flg_in  : IN STD_LOGIC;
			rx_data_in         : IN STD_LOGIC_VECTOR(7 DOWNTO 0); -- Incoming RX data from UART RX
			tx_data_in		   : IN STD_LOGIC_VECTOR(7 DOWNTO 0); -- Data from encryp/decrtypt to be sent to uart tx
			rx_data_ready_in   : IN STD_LOGIC;
			tx_requested_in    : IN STD_LOGIC; -- TX of data was requested by encr/decr controller
			tx_ready_in		   : IN STD_LOGIC;
            tx_done_in         : IN STD_LOGIC; -- TX done signal from UART TX
			-- Outputs
			encr_requested_o   : OUT STD_LOGIC; -- Output signal to flags
            err_flag_o         : OUT STD_LOGIC;
            tx_active_o        : OUT STD_LOGIC; -- Starts TX
			tx_data_o		   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0) -- Data to be sent
		);
	END uart_controller; 

ARCHITECTURE rtl OF uart_controller
 IS

    SIGNAL rx_data_reg        : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL encr_requested_reg : STD_LOGIC; -- Encryption was requested, helper signal for next stage case
    SIGNAL decoding_err_reg   : STD_LOGIC;
    SIGNAL tx_ready_reg       : STD_LOGIC;
    SIGNAL tx_data_reg  	  : STD_LOGIC_VECTOR(7 DOWNTO 0); -- Data sent by encr/decr controller

    type state_type is (S_IDLE, S_TX, S_RX_INCOMING, S_RX_DECODING, S_ERROR, S_TX_WAIT);
    signal state, next_state : state_type; 


    BEGIN 

    sync_state: PROCESS (clk_in) -- synchronous PROCESSes should only have reset and clk in their sensitivity list
    BEGIN
        IF(rising_edge(clk_in)) then
            IF(rst_in = '1') then
                state <= S_IDLE;
            ELSE
                -- Update state of state machine
                state <= next_state;
            END IF;
        END IF;
    END PROCESS sync_state;

	state_output: PROCESS (state, rx_data_reg) -- Add list of input signals to PROCESS sensitivity list
	BEGIN

        rx_data_reg  <= rx_data_in; --REVIEW - Maybe comment out
        tx_ready_reg <= not tx_ready_in;
        tx_data_reg  <= tx_data_in;

        CASE(state) IS
            WHEN S_IDLE =>
							rx_data_reg        <= (OTHERS => '0');
							encr_requested_o   <= '0';
                            encr_requested_reg <= '0';
							decoding_err_reg   <= '0';
                            err_flag_o         <= '0';
                            tx_active_o        <= '0';
                            tx_ready_reg       <= '0';
							tx_data_reg        <= (OTHERS => '0');
                            tx_data_o          <= (OTHERS => '0');

            WHEN S_RX_INCOMING =>
                rx_data_reg  <= rx_data_in;

            WHEN S_RX_DECODING =>
                case rx_data_reg is
									when X"AA" =>
										encr_requested_reg <= '1';
										encr_requested_o   <= '1'; -- SEND request
									when others =>
										decoding_err_reg <= '1';
                END case;
						-- WHEN S_TX_WAIT =>
							
						WHEN S_TX =>
                            tx_active_o <= '1';
							tx_data_o   <= tx_data_reg;
            WHEN S_ERROR =>
                err_flag_o <= '1';
            WHEN OTHERS =>
                null;
        END CASE;
    END PROCESS state_output;


    next_state_proc : PROCESS (ALL) -- Definice prechodu
	BEGIN

    CASE STATE IS
        WHEN S_IDLE =>
            IF rising_edge(rx_data_ready_in) THEN
							next_state <= S_RX_INCOMING;
						ELSIF tx_requested_in = '1' THEN
							next_state <= S_TX_WAIT;
						ELSIF rising_edge(rx_data_ready_in) AND tx_requested_in = '1' THEN -- If both are active RX has a priority
							next_state <= S_RX_INCOMING;
            ELSE
							next_state <= S_IDLE;
            END IF;
				WHEN S_TX_WAIT =>
					IF tx_ready_reg = '1' THEN
						next_state <= S_TX;
					ELSE
						next_state <= S_TX_WAIT;
					END IF;
        WHEN S_TX =>
					IF tx_done_in = '0' THEN
						next_state <= S_TX;
					ELSE 
						next_state <= S_IDLE;
					END IF;
        WHEN S_RX_INCOMING =>
            next_state <= S_RX_DECODING;
        WHEN S_RX_DECODING =>
        IF decoding_err_reg = '1' THEN 
					next_state <= S_ERROR;
				ELSIF encr_requested_reg = '1' THEN
					next_state <= S_IDLE;
				ELSE 
					NULL;
				END IF;
        WHEN S_ERROR =>
					--TODO - flash a led
					next_state <= S_IDLE;
        WHEN OTHERS =>
            null;
    END CASE;
	END PROCESS next_state_proc;
END ARCHITECTURE;




