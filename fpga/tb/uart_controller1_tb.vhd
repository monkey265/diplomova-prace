LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

library main_lib;
    use main_lib.tb_pkg.all;

library fpga_lib;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY uart_controller1_tb IS
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF uart_controller1_tb IS
    
    SIGNAL clk_in_tb           : STD_LOGIC := '0';  
    SIGNAL rst_in_tb           : STD_LOGIC := '0';        
    SIGNAL rx_data_in_tb       : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');    
    SIGNAL tx_data_in_tb	   : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');	  
    SIGNAL rx_data_ready_in_tb : STD_LOGIC := '0';    
    SIGNAL tx_requested_in_tb  : STD_LOGIC := '0';	   
    SIGNAL tx_ready_in_tb	   : STD_LOGIC := '0';	   
    SIGNAL encr_requested_o_tb : STD_LOGIC := '0';    
    SIGNAL err_flag_o_tb       : STD_LOGIC := '0';
    SIGNAL tx_done_tb          : STD_LOGIC := '0';
    SIGNAL tx_active_o_tb      : STD_LOGIC := '0';      
    SIGNAL tx_data_o_tb        : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');

    -- ALIAS decoding_err_reg_tb IS
    --     <<SIGNAL .uart_controller1_tb.uart_controller_inst.decoding_err_reg : STD_LOGIC>>;

    -- Clock period
    CONSTANT clk_period : time := 10 ns;

    BEGIN 

    uart_controller_inst: entity fpga_lib.uart_controller
     port map(
        clk_in           => clk_in_tb,
        rst_in           => rst_in_tb,
        rx_data_in       => rx_data_in_tb,
        tx_data_in       => tx_data_in_tb,
        rx_data_ready_in => rx_data_ready_in_tb,
        tx_requested_in  => tx_requested_in_tb,
        tx_ready_in      => tx_ready_in_tb,
        tx_active_o      => tx_active_o_tb,
        tx_done_in       => tx_done_tb,
        encr_requested_o => encr_requested_o_tb,
        err_flag_o       => err_flag_o_tb,
        tx_data_o        => tx_data_o_tb
    );

            -- Clock process
    clk_process : PROCESS
    BEGIN
        clk_in_tb <= '0';
        WAIT FOR clk_period / 2;
        clk_in_tb <= '1';
        WAIT FOR clk_period / 2;
    END PROCESS;

    stim_proc : PROCESS
    BEGIN
    test_runner_setup(runner, runner_cfg);
        WAIT FOR clk_period;    
        -- Initial reset
        rst_in_tb <= '1';
        wait for 200 ns;
        rst_in_tb <= '0';
        wait for 100 ns;

        -- Test 1
        rx_data_in_tb <= X"FF";
        WAIT FOR 10 ns;
        rx_data_in_tb <= X"DD";
        strobe(sig => rx_data_ready_in_tb, period_ns => 100 ns);
        -- WAIT UNTIL rising_edge(encr_requested_o_tb);
        -- check(err_flag_o_tb = '1', "Expected decoding error at this point");
        rx_data_in_tb <= X"CC";

        rx_data_in_tb <= X"AA";
        WAIT FOR(100 ns);
        strobe(sig => rx_data_ready_in_tb, period_ns => 100 ns);
        check(encr_requested_o_tb = '1', "Expected active encryption request at this point");




        


        test_runner_cleanup(runner);
    END PROCESS;

END ARCHITECTURE behav;