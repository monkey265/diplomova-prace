library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

library main_lib;
use main_lib.tb_pkg.all;

library fpga_lib;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

entity uart_controller3_tb is
    generic (runner_cfg : string);
end uart_controller3_tb;

ARCHITECTURE behav of uart_controller3_tb IS

    constant c_CLKS_PER_BIT : integer := 87;
    constant c_BIT_PERIOD : time := 8680 ns;

    SIGNAL clk_in_tb           : STD_LOGIC := '0';  
    SIGNAL rst_in_tb           : STD_LOGIC := '0';        
    SIGNAL rx_data_in_tb       : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');    
    SIGNAL tx_data_in_tb	   : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');	  
    SIGNAL rx_data_ready_in_tb : STD_LOGIC := '0';    
    SIGNAL tx_requested_in_tb  : STD_LOGIC := '0';	   
    SIGNAL tx_ready_in_tb	   : STD_LOGIC := '0';	   
    SIGNAL encr_requested_o_tb : STD_LOGIC := '0';    
    SIGNAL err_flag_o_tb       : STD_LOGIC := '0';    
    SIGNAL tx_active_o_tb      : STD_LOGIC := '0';    
    SIGNAL tx_data_o_tb        : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
    SIGNAL o_rx_byte_tb        : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');

    -- SIGNAL i_rx_serial         : STD_LOGIC := '0'; 
    SIGNAL tx_serial_tb        : STD_LOGIC := '0'; 
    SIGNAL tx_done_tb          : STD_LOGIC := '0'; 


    BEGIN
    
    UART_RX_inst: entity fpga_lib.UART_RX
    generic map(
       g_CLKS_PER_BIT => c_CLKS_PER_BIT
   )
    port map(
       i_clk       => clk_in_tb,
       i_rx_serial => tx_serial_tb,
       o_rx_dv     => open, -- TODO implement this to the controller
       o_rx_byte   => o_rx_byte_tb
   );

   UART_TX_inst: entity fpga_lib.UART_TX
   generic map(
      g_CLKS_PER_BIT => c_CLKS_PER_BIT
  )
   port map(
        i_Clk       => clk_in_tb,
        i_TX_DV     => tx_active_o_tb,
        i_TX_Byte   => tx_data_o_tb,
        o_TX_Active => tx_ready_in_tb,
        o_TX_Serial => tx_serial_tb,
        o_TX_Done   => tx_done_tb
  );


    uart_controller_inst: entity fpga_lib.uart_controller
    port map(
        clk_in           => clk_in_tb,
        rst_in           => rst_in_tb,
        rx_data_in       => rx_data_in_tb,
        tx_data_in       => tx_data_in_tb,
        rx_data_ready_in => rx_data_ready_in_tb,
        tx_requested_in  => tx_requested_in_tb,
        tx_ready_in      => tx_ready_in_tb,
        tx_active_o      => tx_active_o_tb,
        tx_done_in       => tx_done_tb,
        encr_requested_o => encr_requested_o_tb,
        err_flag_o       => err_flag_o_tb,
        tx_data_o        => tx_data_o_tb
  );

  clk_in_tb <= not clk_in_tb after 10 ns;

    PROCESS IS 
    BEGIN
        test_runner_setup(runner, runner_cfg);

        -- Initial reset
        rst_in_tb <= '1';
        WAIT FOR 200 ns;
        rst_in_tb <= '0';
        WAIT FOR 100 ns;
        
        tx_data_in_tb <= X"CD";
        WAIT FOR(10 ns);
        tx_requested_in_tb <= '1'; -- encryption controller requests
        WAIT UNTIL rising_edge(tx_done_tb);
        my_is_equal(tx_data_o_tb,o_rx_byte_tb); 
        tx_data_in_tb <= X"AB";
        WAIT UNTIL rising_edge(tx_done_tb);
        my_is_equal(tx_data_o_tb,o_rx_byte_tb);
        test_runner_cleanup(runner);
    end process;

    end behav;

