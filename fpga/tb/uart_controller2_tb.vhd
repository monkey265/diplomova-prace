
    library ieee;
    use ieee.std_logic_1164.ALL;
    use ieee.numeric_std.all;

    library main_lib;
    use main_lib.tb_pkg.all;

    library fpga_lib;

    LIBRARY VUNIT_LIB;
    CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

    entity uart_controller2_tb is
    generic (runner_cfg : string);
    end uart_controller2_tb;
    
    architecture behav of uart_controller2_tb is
    
    component uart_tx is
        generic (
        g_CLKS_PER_BIT : integer := 115   -- Needs to be set correctly
        );
        port (
        i_clk       : in  std_logic;
        i_tx_dv     : in  std_logic;
        i_tx_byte   : in  std_logic_vector(7 downto 0);
        o_tx_active : out std_logic;
        o_tx_serial : out std_logic;
        o_tx_done   : out std_logic
        );
    end component uart_tx;
    
    component uart_rx is
        generic (
        g_CLKS_PER_BIT : integer := 87   -- Needs to be set correctly
        );
        port (
        i_clk       : in  std_logic;
        i_rx_serial : in  std_logic;
        o_rx_dv     : out std_logic;
        o_rx_byte   : out std_logic_vector(7 downto 0)
        );
    end component uart_rx;
    
    component uart_controller is
        port (
        -- Inputs
        clk_in             : in  std_logic;
        rst_in             : in  std_logic;
        rx_data_in         : in  std_logic_vector(7 downto 0);
        tx_data_in         : in  std_logic_vector(7 downto 0);
        rx_data_ready_in   : in  std_logic;
        tx_requested_in    : in  std_logic;
        tx_ready_in        : in  std_logic;
        -- Outputs
        encr_requested_o   : out std_logic;
        tx_data_o          : out std_logic_vector(7 downto 0)
        );
    end component uart_controller;
    
    
    -- Test Bench uses a 10 MHz Clock
    -- Want to interface to 115200 baud UART
    -- 10000000 / 115200 = 87 Clocks Per Bit.
    constant c_CLKS_PER_BIT : integer := 87;
    
    constant c_BIT_PERIOD : time := 8680 ns;
    
    signal r_CLOCK     : std_logic                    := '0';
    signal r_TX_DV     : std_logic                    := '0';
    signal r_TX_BYTE   : std_logic_vector(7 downto 0) := (others => '0');
    signal w_TX_SERIAL : std_logic;
    signal w_TX_DONE   : std_logic;
    signal w_RX_DV     : std_logic;
    signal w_RX_BYTE   : std_logic_vector(7 downto 0);
    signal r_RX_SERIAL : std_logic := '1';
    
    -- UART Controller signals
    signal r_RST              : std_logic := '0';
    signal r_TX_REQUESTED     : std_logic := '0';
    signal r_TX_READY         : std_logic := '0';
    signal w_ENCR_REQUESTED   : std_logic := '0';
    signal w_TX_DATA          : std_logic_vector(7 downto 0) := (others => '0');
    signal r_CTRL_TX_DATA_IN  : std_logic_vector(7 downto 0) := (others => '0');
    
    -- Low-level byte-write
    procedure UART_WRITE_BYTE (
        i_data_in       : in  std_logic_vector(7 downto 0);
        signal o_serial : out std_logic) is
    begin
    
        -- Send Start Bit
        o_serial <= '0';
        wait for c_BIT_PERIOD;
    
        -- Send Data Byte
        for ii in 0 to 7 loop
        o_serial <= i_data_in(ii);
        wait for c_BIT_PERIOD;
        end loop;  -- ii
    
        -- Send Stop Bit
        o_serial <= '1';
        wait for c_BIT_PERIOD;
        end UART_WRITE_BYTE;
    
    begin
    
    -- Instantiate UART transmitter 
    UART_TX_INST : uart_tx 
    generic map (
        g_CLKS_PER_BIT => c_CLKS_PER_BIT
        )
        port map (
        i_clk       => r_CLOCK,
        i_tx_dv     => r_TX_DV,
        i_tx_byte   => r_TX_BYTE,
        o_tx_active => open,
        o_tx_serial => w_TX_SERIAL,
        o_tx_done   => w_TX_DONE
        );
    
    -- Instantiate UART Receiver
    UART_RX_INST : uart_rx
        generic map (
        g_CLKS_PER_BIT => c_CLKS_PER_BIT
        )
        port map (
        i_clk       => r_CLOCK,
        i_rx_serial => r_RX_SERIAL,
        o_rx_dv     => w_RX_DV,
        o_rx_byte   => w_RX_BYTE
        );
    
    -- Instantiate UART Controller
    UART_CTRL_INST : uart_controller
        port map (
        clk_in           => r_CLOCK,
        rst_in           => r_RST,
        rx_data_in       => w_RX_BYTE,
        tx_data_in       => r_CTRL_TX_DATA_IN,
        rx_data_ready_in => w_RX_DV,
        tx_requested_in  => r_TX_REQUESTED,
        tx_ready_in      => r_TX_READY,
        encr_requested_o => w_ENCR_REQUESTED,
        tx_data_o        => w_TX_DATA
        );

    r_CLOCK <= not r_CLOCK after 50 ns;
    
    process is
    begin
        test_runner_setup(runner, runner_cfg);
        
        -- Initial reset
        r_RST <= '1';
        wait for 200 ns;
        r_RST <= '0';
        wait for 100 ns;
        
        -- Test 1: Tell the UART to send a command
        wait until rising_edge(r_CLOCK);
        wait until rising_edge(r_CLOCK);
        r_TX_DV   <= '1';
        r_TX_BYTE <= X"AB";
        wait until rising_edge(r_CLOCK);
        r_TX_DV   <= '0';
        wait until w_TX_DONE = '1';
        
        wait for 1 us;
        
        -- Test 2: Send a command to the UART that should trigger encryption request
        wait until rising_edge(r_CLOCK);
        UART_WRITE_BYTE(X"AA", r_RX_SERIAL);  -- Sending AA which should trigger encryption request
        wait until rising_edge(r_CLOCK);
        
        -- Check that encryption was requested
        wait for 20 us;  -- Wait for controller to process
        if w_ENCR_REQUESTED = '1' then
        report "Test Passed - Encryption Requested" severity note;
        else
        report "Test Failed - Encryption Not Requested" severity error;
        end if;
        
        wait for 1 us;
        
        -- Test 3: Test TX path through controller
        wait until rising_edge(r_CLOCK);
        r_CTRL_TX_DATA_IN <= X"EF";  -- Data to be transmitted
        r_TX_REQUESTED <= '1';
        wait until rising_edge(r_CLOCK);
        wait until rising_edge(r_CLOCK);
        
        -- Wait a bit then signal TX is ready
        wait for 2 us;
        r_TX_READY <= '1';
        wait for 200 ns;
        r_TX_READY <= '0';
        
        -- Check output data from controller
        if w_TX_DATA = X"EF" then
        report "Test Passed - TX Data Correctly Passed Through Controller" severity note;
        else
        report "Test Failed - TX Data Not Correctly Passed" severity error;
        end if;
        
        -- End TX request
        wait for 1 us;
        r_TX_REQUESTED <= '0';
        
        wait for 1 us;
        
        -- Test 4: Send an invalid command to test error handling
        wait until rising_edge(r_CLOCK);
        UART_WRITE_BYTE(X"CD", r_RX_SERIAL);  -- Not AA, should cause error state
        wait until rising_edge(r_CLOCK);
        
        wait for 20 us;  -- Wait for controller to process
        
        -- No explicit check for error state, but controller should return to IDLE
        
        test_runner_cleanup(runner);
    end process;
    
    end behav;