library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tea_encrypt_click_top is
    port (
        rst_in        : IN  STD_LOGIC;
        ack_in        : IN  STD_LOGIC;
        encr_start_in : IN  STD_LOGIC;
        start_in      : IN  STD_LOGIC;
        data_in       : IN  STD_LOGIC_VECTOR(63  DOWNTO 0);
        key_in        : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
        data_o        : OUT STD_LOGIC_VECTOR(63  DOWNTO 0);
        encr_done_o   : OUT STD_LOGIC
    );
end tea_encrypt_click_top;

architecture struct of tea_encrypt_click_top is

-- Start component
signal start_component_0_ctrl_out_ack : STD_LOGIC;
signal start_component_0_ctrl_out_req : STD_LOGIC;
signal reg_fork_1_outC_req            : STD_LOGIC;
signal reg_fork_1_outC_ack            : STD_LOGIC;
signal start_net                      : STD_LOGIC;


begin

    -- flow starts here
    -- start_component_inst: entity work.start_component
    -- port map(
    --     start   => start_in,
    --     in_ack  => in_ack,
    --     in_req  => in_req,
    --     out_ack => out_ack,
    --     out_req => out_req
    -- );

    MX_0: entity work.mux
 generic map(
    DATA_WIDTH     => DATA_WIDTH,
    PHASE_INIT_C   => PHASE_INIT_C,
    PHASE_INIT_A   => PHASE_INIT_A,
    PHASE_INIT_B   => PHASE_INIT_B,
    PHASE_INIT_SEL => PHASE_INIT_SEL
)
 port map(
    rst       => rst_in,
    inA_req   => inA_req,
    inA_data  => inA_data,
    inA_ack   => inA_ack,
    inB_req   => inB_req,
    inB_data  => inB_data,
    inB_ack   => inB_ack,
    outC_req  => outC_req,
    outC_data => outC_data,
    outC_ack  => outC_ack,
    inSel_req => inSel_req,
    inSel_ack => inSel_ack,
    selector  => selector
);

demux_inst: entity work.demux
 generic map(
    PHASE_INIT_A => PHASE_INIT_A,
    PHASE_INIT_B => PHASE_INIT_B,
    PHASE_INIT_C => PHASE_INIT_C
)
 port map(
    rst       => rst_in,
    inA_req   => inA_req,
    inA_data  => inA_data,
    inA_ack   => inA_ack,
    inSel_req => inSel_req,
    inSel_ack => inSel_ack,
    selector  => selector,
    outB_req  => outB_req,
    outB_data => outB_data,
    outB_ack  => outB_ack,
    outC_req  => outC_req,
    outC_data => outC_data,
    outC_ack  => outC_ack
);

RF_1: entity work.reg_fork
 generic map(
    DATA_WIDTH   => DATA_WIDTH,
    VALUE        => VALUE,
    PHASE_INIT_A => PHASE_INIT_A,
    PHASE_INIT_B => PHASE_INIT_B,
    PHASE_INIT_C => PHASE_INIT_C
)
 port map(
    rst       => rst_in,
    inA_req   => inA_req,
    inA_data  => inA_data,
    inA_ack   => inA_ack,
    outB_req  => outB_req,
    outB_data => outB_data,
    outB_ack  => outB_ack,
    outC_req  => outC_req,
    outC_data => outC_data,
    outC_ack  => outC_ack
);

END ARCHITECTURE STRUCT;