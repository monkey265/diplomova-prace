LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

    use work.design_pkg.all;

ENTITY TEA_encrypt_async_FSM
    IS 
    PORT(
        fire_in       : IN  STD_LOGIC;
        rst_in        : IN  STD_LOGIC;
        encr_start_in : IN  STD_LOGIC;
        data_in       : IN  STD_LOGIC_VECTOR(63  DOWNTO 0);
        key_in        : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
        data_o        : OUT STD_LOGIC_VECTOR(63  DOWNTO 0);
        encr_done_o   : OUT STD_LOGIC;
        gate_o        : OUT STD_LOGIC
    );
    END TEA_encrypt_async_FSM;

ARCHITECTURE RTL OF TEA_encrypt_async_FSM
    IS
        CONSTANT delta        : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"9E3779B9";
        SIGNAL k0, k1, k2, k3 : STD_LOGIC_VECTOR(31 DOWNTO 0);
        SIGNAL counter_int    : STD_LOGIC_VECTOR(5 DOWNTO 0)  :=(OTHERS => '0');
        SIGNAL sum            : STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');
        SIGNAL v0, v1         : STD_LOGIC_VECTOR(31 DOWNTO 0);
        SIGNAL finished_flg   : STD_LOGIC := '0';

        type state_type is (S_INIT, S_ENCR_START, S_SUM, S_V0, S_V1, S_FINISH);
        signal state, next_state : state_type; 
    BEGIN

    k0           <= key_in(127 DOWNTO 96);
    k1           <= key_in(95 DOWNTO 64);
    k2           <= key_in(63 DOWNTO 32);
    k3           <= key_in(31 DOWNTO 0);
  
    ----------------------------------------------

    sync_state: process (fire_in) -- synchronous processes should only have reset and clk in their sensitivity list
	begin
		if(rising_edge(fire_in)) then
			if(rst_in = '1') then
				state <= S_INIT;
			else
				-- Update state of state machine
				state <= next_state;
			end if;
		end if;
	end process sync_state;
	
    -- Process Description: Update output signals based on current state
	-- Process is asynchronous
	-- Additional details: Add additional details if needed
	state_output: process (state) -- Add list of input signals to process sensitivity list
	BEGIN
    CASE(state) IS
    
      WHEN S_INIT => 
        encr_done_o  <= '0';
        sum          <= (OTHERS => '0');
        data_o       <= (OTHERS => '0');
        v0           <= data_in(63 DOWNTO 32);
        v1           <= data_in(31 DOWNTO 0);
        -- k0           <= key_in(127 DOWNTO 96);
        -- k1           <= key_in(95 DOWNTO 64);
        -- k2           <= key_in(63 DOWNTO 32);
        -- k3           <= key_in(31 DOWNTO 0);
        -- delta        <= X"9E3779B9"; -- maybe change to constant!!!
        gate_o    <= '1';
      WHEN S_ENCR_START =>
        sum          <= (OTHERS => '0');
        k0           <= key_in(127 DOWNTO 96);
        k1           <= key_in(95 DOWNTO 64);
        k2           <= key_in(63 DOWNTO 32);
        k3           <= key_in(31 DOWNTO 0);
        v0           <= data_in(63 DOWNTO 32);
        v1           <= data_in(31 DOWNTO 0);
        finished_flg <= '0';
        gate_o    <= '1';
      WHEN S_SUM =>
        sum       <= std_logic_vector(unsigned(sum) + unsigned(delta));
        gate_o    <= '1';
      WHEN S_V0 =>
        v0        <= std_logic_vector(unsigned(v0) + unsigned(((v1 SLL 4) + k0) XOR (v1 + sum) XOR ((v1 SRL 5) + k1)));
        gate_o    <= '1';
      WHEN S_V1 => 
        v1        <= std_logic_vector(unsigned(v1) + unsigned(((v0 SLL 4) + k2) XOR (v0 + sum) XOR ((v0 SRL 5) + k3)));
        counter_int <= counter_int + "000001";
        gate_o    <= '1';
      WHEN S_FINISH =>
        encr_done_o  <= '1';
        data_o       <= v0 & v1;
        counter_int  <= (OTHERS => '0');
        finished_flg <= '1';
      when others =>
    null;
    end case ;
	end process state_output;
	
	next_state_proc : process (state,encr_start_in,counter_int) -- Definice p?echod?
	begin

    case state is
      when S_INIT =>
        IF rising_edge(encr_start_in) THEN -- maybe change to rising edge
          next_state <= S_ENCR_START;
        ELSE
          next_state <= S_INIT;
        END IF;
      when S_ENCR_START =>
        next_state <= S_SUM;
      when S_SUM =>
        next_state <= S_V0;
      when S_V0 =>
        next_state <= S_V1;
      when S_V1 =>
        COUNTER:IF counter_int = C_TEA_ROUNDS THEN
          next_state <= S_FINISH;
        ELSE 
          next_state <= S_SUM;
      END IF COUNTER;
      WHEN S_FINISH =>
        next_state <= S_INIT;
      when others =>
        null;
    end case;
	end process next_state_proc;

END ARCHITECTURE ;

