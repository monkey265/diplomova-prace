-----------------------------------------------------------
---------- TEA encryption click - Initialization ----------
-----------------------------------------------------------

LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

    use work.design_pkg.all;
    use work.delay_element;

ENTITY tea_encrypt_click_counter IS
    -- generic (
    --     DATA_WIDTH  : natural := DATA_WIDTH);
    PORT (
        rst_in         : IN  std_logic;
        -- Input channel
        in_req          : in  std_logic;
        in_ack          : out std_logic;
        -- Output channel
        out_req         : out std_logic;
        out_ack         : in  std_logic;
        -----------------------
        cnt_out         : out STD_LOGIC_VECTOR(4 downto 0)
    );
    
END tea_encrypt_click_counter;

ARCHITECTURE RTL OF tea_encrypt_click_counter IS
-- Signal assignments

signal connect: std_logic := '0'; -- signal for constraining i/o (needed only for post-timing simulation)
signal cnt_internal : STD_LOGIC_VECTOR(4 downto 0) := (OTHERS => '0');
attribute dont_touch : string;
attribute dont_touch of  connect: signal is "true";
attribute dont_touch of  cnt_internal: signal is "true";

--REVIEW - Might have to do it with internal signals

BEGIN

  delay_req: entity work.delay_element
    generic map(
      size => ADD_DELAY-- Delay  size
    )
    port map (
      d => in_req,
      z => out_req
    );

    cnt_internal <= (OTHERS => '0') when RISING_EDGE(rst_in) else
                    (OTHERS => '0') when cnt_internal > "11111" else
                    cnt_internal + "00001" when RISING_EDGE(in_req) else
                unaffected;

    cnt_out <= cnt_internal;

    in_ack <= out_ack;
END RTL;