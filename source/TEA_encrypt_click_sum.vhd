LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

    use work.design_pkg.all;
    use work.delay_element;

ENTITY tea_encrypt_click_sum IS
    PORT (
        rst_in         : IN  std_logic;
        -- Input channel
        in_req          : in  std_logic;
        in_ack          : out std_logic;
        -- Output channel
        out_req         : out std_logic;
        out_ack         : in  std_logic;
        -----------------------
        cnt_in         : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
        V0,V1          : INOUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        k0, k1, k2, k3 : INOUT std_logic_vector(31 DOWNTO 0);
        sum_out        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
END tea_encrypt_click_sum;

ARCHITECTURE RTL OF tea_encrypt_click_sum IS
-- signal assignments here

signal connect      : std_logic := '0'; -- signal for constraining i/o (needed only for post-timing simulation)
signal sum_internal : STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');
constant delta      : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"9E3779B9";


attribute dont_touch : string;
attribute dont_touch of  connect: signal is "true";
attribute dont_touch of  sum_internal: signal is "true";
--REVIEW - Might have to do it with internal signals

BEGIN

-- in_ack <= out_ack;

    delay_req: entity work.delay_element
        generic map(
        size => ADD_DELAY-- Delay  size
    )
    port map (
        d => in_req,
        z => out_req
    );

    process(in_req, rst_in)
    begin
        IF rising_edge(rst_in) THEN
            sum_internal <= (OTHERS => '0');
        ELSIF cnt_in = "11111" THEN
            sum_internal <= (OTHERS => '0');
        ELSE
            sum_internal <= std_logic_vector(unsigned(sum_internal) + unsigned(delta));
        END IF;

        IF cnt_in = "11111" THEN
            sum_internal <= (OTHERS => '0');
        END IF;
        sum_internal <= sum_out;
    END PROCESS;
    
    -- sum_internal <= (OTHERS => '0') when RISING_EDGE(rst_in) else
--                 -- (OTHERS => '0') when cnt_in = "11111" else
--                 std_logic_vector(unsigned(sum_internal) + unsigned(delta));
    in_ack <= out_ack;
    V0 <= V0;
    V1 <= V1;
    k0 <= k0;
    k1 <= k1;
    k2 <= k2;
    k3 <= k3;
END RTL;