LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

PACKAGE design_pkg IS
    -- Functions
	FUNCTION IS_ZEROS (data_in : std_logic_vector) RETURN BOOLEAN;

	CONSTANT DATA_WIDTH : Integer := 16;

	--Delay size
	CONSTANT ADD_DELAY      : integer := 15;
	CONSTANT LUT_CHAIN_SIZE : integer := 10;
	CONSTANT AND2_DELAY     : time := 2 ns; -- 2 input AND gate
	CONSTANT AND3_DELAY     : time := 3 ns; -- 3 input AND gate
	CONSTANT NOT1_DELAY     : time := 1 ns; -- 1 input NOT gate
	CONSTANT ANDOR3_DELAY   : time := 4 ns; -- Complex AND_OR gate
	CONSTANT REG_CQ_DELAY   : time := 1 ns; -- Clk TO Q delay
	CONSTANT ADDER_DELAY    : time := 15 ns; -- Adder delay
	
	CONSTANT OR2_DELAY      : time := 2 ns; -- 2 input OR gate
	CONSTANT XOR_DELAY	    : time := 3 ns; --2 input XOR gate

	CONSTANT C_TEA_ROUNDS   	: natural := 32;
END PACKAGE design_pkg;

PACKAGE BODY design_pkg IS
FUNCTION IS_ZEROS (data_in : std_logic_vector) RETURN BOOLEAN IS
	BEGIN
		FOR i IN data_in'RANGE LOOP
            IF data_in(i) = '1' THEN
                RETURN FALSE;
            END IF;
        END LOOP;
        RETURN TRUE;
	END FUNCTION;
   --TODO make synthesis friendly version
END PACKAGE BODY design_pkg;
