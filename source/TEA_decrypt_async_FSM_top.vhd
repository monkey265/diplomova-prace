library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity TEA_decrypt_async_FSM_top IS
    PORT(
        trig_i        : IN     STD_LOGIC;
        rst_in        : IN     STD_LOGIC;
        decr_start_in : IN     STD_LOGIC;
        in_ack        : INOUT  STD_LOGIC;
        data_in       : IN     STD_LOGIC_VECTOR(63  DOWNTO 0);
        key_in        : IN     STD_LOGIC_VECTOR(127 DOWNTO 0);
        data_o        : OUT    STD_LOGIC_VECTOR(63  DOWNTO 0);
        decr_done_o   : OUT    STD_LOGIC
    );
END ENTITY TEA_decrypt_async_FSM_top;

ARCHITECTURE STRUCT OF TEA_decrypt_async_FSM_top IS

SIGNAL trg_gate_xored : STD_LOGIC;
SIGNAL delay_net      : STD_LOGIC;
SIGNAL fire_net       : STD_LOGIC;
SIGNAL gate_net       : STD_LOGIC;

component my_click_element is
    port (
        rst_in    : IN  std_logic   := '0';
        -- Input channel
        in_ack    : INOUT std_logic := '0';
        in_req    : IN  std_logic   := '0';
        -- Output channel
        out_req   : INOUT std_logic := '0';
        out_ack   : IN  std_logic   := '0';
    
        out_fire  : OUT std_logic   := '0'
    );
end component;

component TEA_decrypt_async_FSM is
    port (
        fire_in       : IN  STD_LOGIC;
        rst_in        : IN  STD_LOGIC;
        decr_start_in : IN  STD_LOGIC;
        data_in       : IN  STD_LOGIC_VECTOR(63  DOWNTO 0);
        key_in        : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
        data_o        : OUT STD_LOGIC_VECTOR(63  DOWNTO 0);
        decr_done_o   : OUT STD_LOGIC;
        gate_o        : OUT STD_LOGIC
    );
end component;

BEGIN

trg_gate_xored <= gate_net xor trig_i;

my_click_element_inst: my_click_element
port map(
    rst_in    => rst_in,
    in_ack    => in_ack,
    in_req    => trg_gate_xored,
    out_req   => delay_net,
    out_ack   => delay_net,
    out_fire  => fire_net
);

TEA_decrypt_async_FSM_inst: TEA_decrypt_async_FSM
 port map(
    fire_in       => fire_net,
    rst_in        => rst_in,
    decr_start_in => decr_start_in,
    data_in       => data_in,
    key_in        => key_in,
    data_o        => data_o,
    decr_done_o   => decr_done_o,
    gate_o        => gate_net
);



END ARCHITECTURE STRUCT;