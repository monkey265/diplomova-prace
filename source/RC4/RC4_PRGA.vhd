LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

  LIBRARY main_lib;
    use main_lib.design_pkg.all;

ENTITY RC4_PRGA
IS
    GENERIC (N : integer := 256);
    PORT ( 
      clk_in          : IN STD_LOGIC;
      rst_in          : IN STD_LOGIC;
      key_start_in    : IN STD_LOGIC; -- Starts key loading from PRGA 
      plaintext_load  : IN STD_LOGIC; -- Starts plaintext loading 
      key_in          : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      plaintext_in    : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
    );
END RC4_PRGA;

ARCHITECTURE rtl OF RC4_PRGA IS
  TYPE state_type IS (S_INIT, S_KEY_LOADING, S_KEY_INCR, S_PLAINTEXT_LOAD, S_PLAINTEXT_INCR, S_PLAINTEXT_WAIT);
  TYPE s_array IS array (0 to 255) of STD_LOGIC_VECTOR(7 downto 0);
  SIGNAL state, next_state : state_type;
  SIGNAL S_reg             : s_array;
  SIGNAL P_reg             : s_array;
  SIGNAL i, j, key_cntr    : integer range 0 to N;
  SIGNAL plaintxt_cntr     : integer range 0 to N;
BEGIN 


  sync_state: PROCESS (clk_in, rst_in) -- synchronous processes should only have reset and clk in their sensitivity list
  BEGIN
    IF(rising_edge(clk_in)) then
      IF(rst_in = '1') then
        state <= S_INIT;
      ELSE
        state <= next_state;
      END IF;
    END IF;
  END PROCESS sync_state;

  state_output:PROCESS(state) --REVIEW - i,j on sensitivity lISt?
  BEGIN
  IF RISING_EDGE(clk_in) THEN --SECTION - Clock if
    CASE state IS
      WHEN S_INIT =>
        i <= 0;
        j <= 0;
        plaintxt_cntr <= 0;
        key_cntr      <= 0;
      WHEN S_KEY_LOADING =>
        IF RISING_EDGE(key_start_in) THEN
          S_reg(key_cntr) <= key_in;
        END IF;
      WHEN S_KEY_INCR =>
        key_cntr <= key_cntr + 1;
      WHEN S_PLAINTEXT_WAIT =>
        null; -- Just waiting for trigger
      WHEN S_PLAINTEXT_LOAD =>
        P_reg(plaintxt_cntr) <= plaintext_in;
      WHEN S_PLAINTEXT_INCR =>
        plaintxt_cntr <= plaintxt_cntr + 1;
    END CASE;
  END IF; --!SECTION Clock if
  END PROCESS;


    -- **FSM Operations**
    next_state_proc : PROCESS(state)
    BEGIN
    CASE (state) IS
      WHEN S_INIT =>
        IF RISING_EDGE(key_start_in) THEN
          next_state <= S_KEY_LOADING;
        ELSE
          next_state <= S_INIT;
        END IF;
        WHEN S_KEY_LOADING =>
        WHEN S_KEY_INCR =>
          IF key_cntr = N - 1 THEN
            next_state <= S_KEY_LOADING;
          ELSE
            next_state <= S_PLAINTEXT_WAIT;
          END IF;
        WHEN S_PLAINTEXT_WAIT =>
          IF RISING_EDGE(plaintext_load) THEN
            next_state <= S_PLAINTEXT_LOAD;
          ELSE
            next_state <= S_PLAINTEXT_WAIT;
          END IF;
        WHEN S_PLAINTEXT_LOAD =>
          next_state <= S_PLAINTEXT_INCR;
        WHEN S_PLAINTEXT_INCR =>
          IF plaintxt_cntr = N THEN
            next_state <= next_state;
          ELSE 
            next_state <= S_PLAINTEXT_LOAD;
          END IF;
    END CASE;
  END PROCESS;

END rtl;
