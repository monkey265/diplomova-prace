LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

  LIBRARY main_lib;
    use main_lib.design_pkg.all;



entity RC4_KSA 
 IS
    generic (N : integer := 256; key_in_len : integer := 256);
    port (
        clk_in          : IN  STD_LOGIC;
        rst_in          : IN  STD_LOGIC;
        start_in        : IN  STD_LOGIC;
        key_in          : IN  STD_LOGIC_VECTOR(7 downto 0);
        key_req_o       : OUT STD_LOGIC;
        S_o             : OUT STD_LOGIC_VECTOR(7 downto 0);
        key_unloading_o : OUT STD_LOGIC; -- signalizes that key starts to unload
        key_done_o      : OUT STD_LOGIC
    );
END RC4_KSA;

architecture rtl of RC4_KSA IS
    type state_type IS (S_INIT,S_KEY_REQ, S_FILL_S, S_SWAP1, S_SWAP2, S_SWAP3, S_PERMUTE_S, S_UNLOADING, S_DONE, S_MOD_PREP,S_KEY_LOADING, S_KEY_INCR, S_SWAP_INCR, S_UNLOADING_INCR, S_UNLOADING_REQ);
    SIGNAL state, next_state : state_type; 

    TYPE s_array IS array (0 to 255) of STD_LOGIC_VECTOR(7 downto 0);
    TYPE k_array IS array (0 to 63)  of STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL S_reg             : s_array;
    SIGNAL key_reg           : k_array;
    SIGNAL i, j              : integer range 0 to N;
    SIGNAL temp              : std_logic_vector(7 downto 0);
    SIGNAL key_internal      : std_logic_vector(7 downto 0);
    SIGNAL counter_int       : integer range 0 to N := 0;
    SIGNAL key_counter       : natural range 0 to 64;
    SIGNAL unloading_counter : natural range 0 to N;
    SIGNAL array_position    : natural range 0 to N - 1;
  BEGIN

    sync_state: PROCESS (clk_in, rst_in) -- synchronous processes should only have reset and clk in their sensitivity list
	BEGIN
		IF(RISING_EDGE(clk_in)) THEN
			IF(rst_in = '1') THEN
				state <= S_INIT;
			ELSE
				state <= next_state;
			END IF;
		END IF;
	END PROCESS sync_state;

  state_output:PROCESS(state, i, j, start_in, clk_in,key_counter) --REVIEW - i,j on sensitivity lISt?
	BEGIN
  key_internal <= key_in;
  IF RISING_EDGE(clk_in) THEN --
      CASE state IS
        WHEN S_INIT =>
          i <= 0;
          j <= 0;
          counter_int       <= 0;
          key_done_o        <= '0';
          unloading_counter <= 0;
          key_counter       <= 0;
          key_req_o         <= '0';
          key_unloading_o   <= '0';
          S_o               <= (others => '0');
        WHEN S_KEY_REQ =>
          key_req_o <= '1';
        WHEN S_KEY_LOADING =>
          key_reg(key_counter) <= key_in;
        WHEN S_KEY_INCR =>
          key_counter <= key_counter + 1;
          key_req_o <= '0';
        WHEN S_FILL_S =>  -- filling
          S_reg(i) <= std_logic_vector(to_unsigned(i, 8));
          i <= i + 1;
        WHEN S_MOD_PREP =>
          array_position <= (counter_int mod 64); --TODO - maybe merge it into S_PERMUTE_S
        WHEN S_PERMUTE_S =>
            -- j <= (j + (S_reg(counter_int)) + (key_in(counter_int mod key_in_len))) mod N;
            -- j <= (j + TO_INTEGER(UNSIGNED(S_reg(counter_int))) + (TO_INTEGER(UNSIGNED(key_in)) mod key_in_len)) mod N;
            -- j <= (j + TO_INTEGER(UNSIGNED(S_reg(counter_int))) + (TO_INTEGER(UNSIGNED(key_internal(counter_int mod key_in_len downto counter_int mod key_in_len))))) mod N;
            -- j <= (j + TO_INTEGER(UNSIGNED(S_reg(counter_int))) + 
            -- TO_INTEGER(UNSIGNED(key_internal(((counter_int mod key_in_len)+1)*8-1 downto (counter_int mod key_in_len)*8)))) mod N;
            j <= (j + TO_INTEGER(UNSIGNED(S_reg(counter_int))) + (TO_INTEGER(UNSIGNED(key_reg(array_position))))) mod N;
            WHEN S_SWAP1 =>
          temp <= S_reg(counter_int);
        WHEN S_SWAP2 =>
          S_reg(counter_int) <= S_reg(j);
        WHEN S_SWAP3 =>
          S_reg(j) <= temp;
        WHEN S_SWAP_INCR =>
          counter_int <= counter_int + 1;
        WHEN S_UNLOADING_REQ =>
          key_unloading_o <= '1';
        WHEN S_UNLOADING =>
          S_o <= S_reg(unloading_counter);
        WHEN S_UNLOADING_INCR =>
          key_unloading_o <= '0';
          unloading_counter <= unloading_counter + 1;
        WHEN S_DONE =>
            key_done_o <= '1';
        WHEN others =>
            null;
    END CASE;
    END IF;
  END PROCESS;

    -- **FSM Operations**
    next_state_proc : PROCESS(state, counter_int,start_in, i, unloading_counter)
    BEGIN
      CASE(state) IS
      WHEN S_INIT =>
        IF RISING_EDGE(start_in) THEN
          next_state <= S_KEY_REQ;
        ELSE 
          next_state <= S_INIT;
        END IF;
      WHEN S_KEY_REQ =>
        next_state <= S_KEY_LOADING;
      WHEN S_KEY_LOADING =>
        next_state <= S_KEY_INCR;
      WHEN S_KEY_INCR =>
        IF key_counter = 63 THEN 
          next_state <= S_FILL_S;
        ELSE 
          next_state <= S_KEY_REQ;
        END IF;
      WHEN S_FILL_S =>
          IF i = 255 THEN 
            next_state  <= S_MOD_PREP;
          ELSE
            next_state <= S_FILL_S;
          END IF;
      
      WHEN S_MOD_PREP =>
        next_state <= S_PERMUTE_S;
      WHEN S_PERMUTE_S =>
        next_state <= S_SWAP1;
      WHEN S_SWAP1 =>
        next_state <= S_SWAP2; 
      WHEN S_SWAP2 =>
        next_state <= S_SWAP3;
      WHEN S_SWAP3 =>
        next_state <= S_SWAP_INCR;
      WHEN S_SWAP_INCR =>
        IF counter_int = N-1 THEN
          next_state <= S_UNLOADING;
        ELSE
          next_state <= S_MOD_PREP;
        END IF;
        WHEN S_UNLOADING_REQ =>
        next_state <= S_UNLOADING;
        WHEN S_UNLOADING =>
          next_state <= S_UNLOADING_INCR;
        WHEN S_UNLOADING_INCR =>
          IF unloading_counter = N-1 THEN
            next_state <= S_DONE;
          ELSE
            next_state <= S_UNLOADING_REQ;
          END IF;
      WHEN S_DONE =>
          next_state <= S_INIT;
      WHEN others =>
          next_state <= S_INIT;
    END CASE;
  END PROCESS;

    -- Output S-array contents
    -- std_logic_vector(to_unsigned(a, number_of_bits)) 

      -- assertion to report S_reg value on every state change

END rtl;
