library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.design_pkg.all;

entity my_click_element is
  port (
    rst_in    : IN  std_logic   := '0';
    -- Input channel
    in_ack    : INOUT std_logic := '0';
    in_req    : IN  std_logic   := '0';
    -- Output channel
    out_req   : INOUT std_logic := '0';
    out_ack   : IN  std_logic   := '0';

    out_fire  : OUT std_logic   := '0'
    );
end my_click_element;

    architecture behavioral of my_click_element is

        signal click    : std_logic := '0';
        signal xored_i  : std_logic := '0';
        signal xnored_i : std_logic := '0';
        signal phase_net: std_logic := '0';

        
        attribute dont_touch : string; 
        attribute dont_touch of  click     : signal is "true";
        attribute dont_touch of  xnored_i  : signal is "true";
        attribute dont_touch of  xored_i   : signal is "true";
        attribute dont_touch of  phase_net : signal is "true";

        -- component DFF_async is
        --     port(
        --         q_o         : out std_logic := '0';    
        --         clk_in      : in  std_logic := '0';  
        --         rst_in      : in  std_logic := '0';  
        --         d_in        : in  std_logic := '0'    
        --     );
        -- end component DFF_async;
    begin

    in_ack  <= phase_net;
    out_req <= phase_net;
    -- xored_i   <= in_req  xor in_ack;
    -- xnored_i  <= out_req xnor out_ack;

    -- out_fire <= xored_i AND xnored_i;

    click <= (not(in_req) and phase_net and out_ack) or (not(out_ack) and not(phase_net) and in_req) after AND3_DELAY + OR2_DELAY;


    -- DFF_async_inst: entity work.DFF_async
    --  port map(
    --     q_o    => phase_net,     
    --     clk_in => out_fire,
    --     rst_in => rst_in,
    --     d_in   => not phase_net
    -- );

clock_regs: process(click, rst_in)
begin
  if rst_in = '1' then
    phase_net <= '0';
  elsif rising_edge(click) then
    phase_net <= not phase_net after REG_CQ_DELAY;
  end if;
end process;

out_fire <= click;

    end behavioral;