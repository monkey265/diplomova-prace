LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

ENTITY c_element IS
  PORT (
    data_in  : IN    std_logic_vector(1 DOWNTO 0);
    data_out : OUT   std_logic
  );
END ENTITY c_element;

ARCHITECTURE arch OF c_element IS

BEGIN

  PROCESS (ALL) IS
  BEGIN
    CASE data_in IS
      WHEN "00" =>
        data_out <= '0';
      WHEN "01" =>
        NULL;            
      WHEN "10" =>
        NULL;           
      WHEN "11" =>
        data_out <= '1';
      WHEN OTHERS =>
        NULL;
    END CASE;
    
  END PROCESS;
  -- Change this from 'X' to 1
  -- psl test_assrt : assert always (data_out /= 'X');

END ARCHITECTURE arch;
