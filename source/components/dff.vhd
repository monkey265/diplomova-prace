----------------------------------------------------------------------------------------------------------------
--------------------------- DFF for use in click element ---------------------------------------------
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.design_pkg.all;

-- DFF with asynchrnonous reset high
ENTITY DFF_async IS
    port(
        q_o         : out std_logic := '0';    
        clk_in      : in  std_logic := '0';  
        rst_in      : in  std_logic := '0';  
        d_in        : in  std_logic := '0'    
    );
END DFF_async;

architecture RTL OF DFF_async IS
  BEGIN 

    process(clk_in,rst_in)
        BEGIN
        if (rst_in = '1') then
            q_o <= '0';
        elsif rising_edge(clk_in) then
            q_o <= d_in;
        END IF;
    END PROCESS;
END RTL;