-----------------------------------------------------------
---------- TEA encryption click - Initialization ----------
-----------------------------------------------------------

LIBRARY ieee ;
    USE ieee.std_logic_1164.ALL ;
    USE IEEE.std_logic_unsigned.ALL;
    USE ieee.numeric_std.ALL ;
    USE IEEE.MATH_REAL.ALL;

    use work.design_pkg.all;
    use work.delay_element;

ENTITY tea_encrypt_click_init IS
    generic (
        DATA_WIDTH  : natural := DATA_WIDTH);
    PORT (
        rst_in         : IN  std_logic;
        -- Input channel
        in_req          : in  std_logic;
        in_ack          : out std_logic;
        -- Output channel
        out_req         : out std_logic;
        out_ack         : in  std_logic;
        -----------------------
        data_in        : IN std_logic_vector(63 DOWNTO 0);
        key_in         : IN std_logic_vector(127 DOWNTO 0);
        v0             : OUT std_logic_vector(31 DOWNTO 0);
        v1             : OUT std_logic_vector(31 DOWNTO 0);
        k0, k1, k2, k3 : OUT std_logic_vector(31 DOWNTO 0)
    );
END tea_encrypt_click_init;

ARCHITECTURE RTL OF tea_encrypt_click_init IS
-- Signal assignments

signal connect: std_logic := '0'; -- signal for constraining i/o (needed only for post-timing simulation)

attribute dont_touch : string;
attribute dont_touch of  connect: signal is "true";
--REVIEW - Might have to do it with internal signals

BEGIN
-- in_ack <= out_ack;

  delay_req: entity work.delay_element
    generic map(
      size => ADD_DELAY-- Delay  size
    )
    port map (
      d => in_req,
      z => out_req
    );

    --REVIEW - Not sure how big delay should be when assigning
    k0 <= key_in(127 DOWNTO 96) after ADDER_DELAY;
    k1 <= key_in(95 DOWNTO 64)  after ADDER_DELAY;
    k2 <= key_in(63 DOWNTO 32)  after ADDER_DELAY;
    k3 <= key_in(31 DOWNTO 0)   after ADDER_DELAY;

    v0 <= data_in(63 DOWNTO 32) after ADDER_DELAY;
    v1 <= data_in(31 DOWNTO 0)  after ADDER_DELAY;

    in_ack <= out_ack;
    
END RTL;