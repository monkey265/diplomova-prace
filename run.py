#!/usr/bin/env python3

import os
from vunit import VUnit
from pathlib import Path

# Create VUnit instance by parsing command line arguments
VU = VUnit.from_argv()  # Do not use compile_builtins.
VU.add_vhdl_builtins()  # Add the VHDL builtins explicitly!
VU.add_verification_components()
VU.add_external_library("unisim", "vivado_libs/unisim")

# Create libraries

main_lib = VU.add_library("main_lib")
fpga_lib = VU.add_library("fpga_lib")

# Adding libraries
main_lib.add_source_files("source/*.vhd")
main_lib.add_source_files("source/*/*.vhd")
main_lib.add_source_files("verification/*.vhd")

fpga_lib.add_source_files("fpga/sources/*.vhd")
fpga_lib.add_source_files("fpga/components/*.vhd")
fpga_lib.add_source_files("fpga/sources/*.vhd")
fpga_lib.add_source_files("fpga/tb/*.vhd")

VU.set_compile_option("modelsim.vcom_flags",["-check_synthesis", "-assertdebug", "-fsmdebug", "-pslext"])

# Ensure wave file contains "run -all"
def ensure_run_all(wave_file_path):
    if not wave_file_path.exists():
        wave_file_path.parent.mkdir(parents=True, exist_ok=True)
        with open(wave_file_path, "w") as wave_file:
            wave_file.write("run -all\n")
    else:
        with open(wave_file_path, "r+") as wave_file:
            lines = wave_file.readlines()
            if not any(line.strip() == "run -all" for line in lines):
                wave_file.write("run -all\n")

# Add waveform automatically when running in GUI mode.
for tb in main_lib.get_test_benches():
    wave_file_path = Path("waves/") / f"{tb.name}_wave.do"
    ensure_run_all(wave_file_path)
    tb.set_sim_option("modelsim.init_file.gui", str(wave_file_path))
    tb.set_sim_option("modelsim.vsim_flags.gui", ["-debugdb=+acc", "-psl"])

for tb in fpga_lib.get_test_benches():
    wave_file_path = Path("waves/") / f"{tb.name}_wave.do"
    ensure_run_all(wave_file_path)
    tb.set_sim_option("modelsim.init_file.gui", str(wave_file_path))
    tb.set_sim_option("modelsim.vsim_flags.gui", ["-debugdb=+acc" "-psl"])

VU.main()
