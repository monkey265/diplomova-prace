LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY main_lib;
    use main_lib.tb_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY RC4_2_tb IS
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF RC4_2_tb IS 

CONSTANT N : INTEGER := 256;

SIGNAL clk_in_tb        : STD_LOGIC;
SIGNAL rst_in_tb        : STD_LOGIC;
SIGNAL key_start_in_tb  : STD_LOGIC;
SIGNAL plaintext_load_tb: STD_LOGIC;
SIGNAL key_in_tb        : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL plaintext_in_tb  : STD_LOGIC_VECTOR(7 DOWNTO 0);



BEGIN

    RC4_PRGA_inst: entity work.RC4_PRGA
    generic map(
    N => N
    )
    port map(
    clk_in         => clk_in_tb,
    rst_in         => rst_in_tb,
    key_start_in   => key_start_in_tb,
    plaintext_load => plaintext_load_tb,
    key_in         => key_in_tb,
    plaintext_in   => plaintext_in_tb
    );

    PROCESS IS 
    BEGIN
        test_runner_setup(runner, runner_cfg);
        -- Initial reset
        strobe(rst_in_tb, 100 ns);
        WAIT FOR 10 us;


        test_runner_cleanup(runner);
    END PROCESS;
END behav;
