LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

use work.tb_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY TEA_decrypt1_tb IS
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF TEA_decrypt1_tb IS

    -- Component declaration for the TEA_decrypt entity
    COMPONENT TEA_decrypt
        PORT (
            clk_in        : IN  STD_LOGIC;
            rst_in        : IN  STD_LOGIC;
            decr_start_in : IN  STD_LOGIC;
            data_in       : IN  STD_LOGIC_VECTOR(63  DOWNTO 0);
            key_in        : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
            data_o        : OUT STD_LOGIC_VECTOR(63  DOWNTO 0);
            decr_done_o   : OUT STD_LOGIC
        );
    END COMPONENT;

    -- Signals for the testbench
    SIGNAL clk_in_tb        : std_logic := '0';
    SIGNAL rst_in_tb        : std_logic := '0';
    SIGNAL data_in_tb       : std_logic_vector(63 downto 0)  := (others => '0');
    SIGNAL key_in_tb        : std_logic_vector(127 downto 0) := (others => '0');
    SIGNAL data_o_tb        : std_logic_vector(63 downto 0)  := (others => '0');
    SIGNAL decr_done_o_tb   : STD_LOGIC := '0';
    SIGNAL decr_start_in_tb : STD_LOGIC := '0';

    -- Clock period
    CONSTANT clk_period : time := 10 ns;

    -- Test case data.  Input is now the *encrypted* data.
    TYPE test_case_t IS RECORD
        encrypted_data : STD_LOGIC_VECTOR(63 DOWNTO 0);
        key_in         : STD_LOGIC_VECTOR(127 DOWNTO 0);
        expected_data  : STD_LOGIC_VECTOR(63 DOWNTO 0);
    END RECORD;

    TYPE test_case_array IS ARRAY (NATURAL RANGE <>) OF test_case_t;

    CONSTANT test_cases : test_case_array := (
        (x"CF4BBB814C933FDA", x"00000000000000000000000000000000", x"ABCDEF1234565612"),  
        (x"5559B4AC70454796", x"000102030405060708090A0B0C0D0E0F", x"2DA5382F24240AD3"),
        (x"2037EB5D4FF730A4", x"000102030405060708090A0B0C0D0E0F", x"E201F06FEBB34F39"),
        (x"FC8A068B3D17F063", x"00000000000000000000000000000000", x"0123456789ABCDEF"), 
        (x"14F0C75D2BEBD98D", x"000102030405060708090A0B0C0D0E0F", x"0123456789ABCDEF"),
        (x"AC7A5E9FD33632B7", x"0000000089ABCDEF0000000089ABCDEF", x"0123456789ABCDEF")
    );  --encrypted data            key in                          expected


BEGIN
    
    uut : TEA_decrypt
        PORT MAP(
            clk_in           => clk_in_tb,
            rst_in           => rst_in_tb,
            data_in          => data_in_tb,
            key_in           => key_in_tb,
            data_o           => data_o_tb,
            decr_done_o      => decr_done_o_tb,
            decr_start_in    => decr_start_in_tb
        );

    -- Clock process
    clk_process : PROCESS
    BEGIN
        clk_in_tb <= '0';
        WAIT FOR clk_period / 2;
        clk_in_tb <= '1';
        WAIT FOR clk_period / 2;
    END PROCESS;

    -- Test process
    stim_proc : PROCESS
    BEGIN
    test_runner_setup(runner, runner_cfg);
        WAIT FOR clk_period;

        FOR i IN test_cases'RANGE LOOP
            REPORT "TC" & integer'image(i+1);
            data_in_tb <= test_cases(i).encrypted_data;
            key_in_tb  <= test_cases(i).key_in;
            decr_start_in_tb <= '1';
            WAIT FOR clk_period;
            decr_start_in_tb <= '0';
            WAIT UNTIL rising_edge(decr_done_o_tb  );  -- Wait for decryption to finish
            check_equal(data_o_tb,test_cases(i).expected_data,"Test");
            WAIT FOR 2*clk_period; 
        END LOOP;

        WAIT FOR clk_period * 32;
        decr_start_in_tb <= '0';


        test_runner_cleanup(runner);
    END PROCESS;

END ARCHITECTURE behav;
