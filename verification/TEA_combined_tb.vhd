LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY TEA_combined_tb IS
    generic (runner_cfg : string);
END ENTITY;

    ARCHITECTURE behav OF TEA_combined_tb IS

    COMPONENT tea_encrypt
    PORT (
        clk_in        : IN  STD_LOGIC;
        rst_in        : IN  STD_LOGIC;
        encr_start_in : IN  STD_LOGIC;
        data_in       : IN  STD_LOGIC_VECTOR(63  downto 0);
        key_in        : IN  STD_LOGIC_VECTOR(127 downto 0);
        data_o        : OUT STD_LOGIC_VECTOR(63  downto 0);
        encr_done_o   : INOUT STD_LOGIC 
    );
    END COMPONENT;

    COMPONENT TEA_decrypt
    PORT (
        clk_in        : IN  STD_LOGIC;
        rst_in        : IN  STD_LOGIC;
        decr_start_in : IN  STD_LOGIC;
        data_in       : IN  STD_LOGIC_VECTOR(63  DOWNTO 0);
        key_in        : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
        data_o        : OUT STD_LOGIC_VECTOR(63  DOWNTO 0);
        decr_done_o   : OUT STD_LOGIC
    );
    END COMPONENT;

        -- Signals for the testbench
        SIGNAL clk_in_tb               : std_logic := '0';
        SIGNAL rst_in_tb               : std_logic := '0';
        SIGNAL data_in_tb              : std_logic_vector(63 downto 0)  := (others => '0');
        SIGNAL data_encrypted_tb       : std_logic_vector(63 downto 0)  := (others => '0');
        SIGNAL data_decrypted_tb       : std_logic_vector(63 downto 0)  := (others => '0');
        SIGNAL key_in_tb               : std_logic_vector(127 downto 0) := (others => '0');
        SIGNAL decr_done_o_tb          : STD_LOGIC := '0';
        SIGNAL decr_start_in_tb        : STD_LOGIC := '0';
        SIGNAL encr_done_o_tb          : STD_LOGIC := '0';
        SIGNAL encr_start_in_tb        : STD_LOGIC := '0';

        -- Clock period 
    CONSTANT clk_period : time := 10 ns;

    TYPE test_case_t IS RECORD
        encrypted_data : STD_LOGIC_VECTOR(63 DOWNTO 0);
        key_in         : STD_LOGIC_VECTOR(127 DOWNTO 0);
        expected_data  : STD_LOGIC_VECTOR(63 DOWNTO 0);
    END RECORD;
    TYPE test_case_array IS ARRAY (NATURAL RANGE <>) OF test_case_t;

    CONSTANT test_cases : test_case_array := (
        (x"ABCDEF1234565612", x"00000000000000000000000000000000", x"ABCDEF1234565612"), 
        (x"0123456789ABCDEF", x"00000000000000000000000000000000", x"0123456789ABCDEF"),
        (x"FC8A068B3D17F063", x"0000000089ABCDEF0000000089ABCDEF", x"FC8A068B3D17F063"),
        (x"14F0C75D2BEBD98D", x"000102030405060708090A0B0C0D0E0F", x"14F0C75D2BEBD98D")
        -- data in              key in                                 expected data
    );

    BEGIN

    uut2 : tea_encrypt
    PORT MAP(
        clk_in           => clk_in_tb,
        rst_in           => rst_in_tb,
        data_in          => data_in_tb,
        key_in           => key_in_tb,
        data_o           => data_encrypted_tb,
        encr_done_o      => encr_done_o_tb,
        encr_start_in    => encr_start_in_tb
    );

    uut : TEA_decrypt
    PORT MAP(
        clk_in           => clk_in_tb,
        rst_in           => rst_in_tb,
        data_in          => data_encrypted_tb,
        key_in           => key_in_tb,
        data_o           => data_decrypted_tb,
        decr_done_o      => decr_done_o_tb,
        decr_start_in    => encr_done_o_tb
    );

    clk_process : PROCESS
        BEGIN
            clk_in_tb <= '0';
            WAIT FOR clk_period / 2;
            clk_in_tb <= '1';
            WAIT FOR clk_period / 2;
        END PROCESS;
    
    stim_proc : PROCESS
        BEGIN
        test_runner_setup(runner, runner_cfg);
        WAIT FOR clk_period;

        FOR i IN test_cases'RANGE LOOP
            REPORT "TC" & integer'image(i+1);
            data_in_tb <= test_cases(i).encrypted_data;
            key_in_tb  <= test_cases(i).key_in;
            encr_start_in_tb <= '1';
            WAIT UNTIL rising_edge(decr_done_o_tb  );  -- Wait for decryption to finish
            check_equal(data_decrypted_tb,test_cases(i).expected_data,"Test");
            encr_start_in_tb <= '0';
            WAIT FOR clk_period;
        END LOOP;

        WAIT FOR clk_period * 32;
        decr_start_in_tb <= '0';

    test_runner_cleanup(runner);
    END PROCESS;

END ARCHITECTURE behav;
