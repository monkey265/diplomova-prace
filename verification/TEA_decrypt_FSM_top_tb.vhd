LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use work.tb_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY TEA_decrypt_async_FSM_top_tb IS
    GENERIC (runner_cfg : STRING);
END ENTITY;

ARCHITECTURE behav OF TEA_decrypt_async_FSM_top_tb IS

    COMPONENT TEA_decrypt_async_FSM_top
        PORT (
            trig_i        : IN     STD_LOGIC;
            rst_in        : IN     STD_LOGIC;
            decr_start_in : IN     STD_LOGIC;
            in_ack        : INOUT  STD_LOGIC;
            data_in       : IN     STD_LOGIC_VECTOR(63  DOWNTO 0);
            key_in        : IN     STD_LOGIC_VECTOR(127 DOWNTO 0);
            data_o        : OUT    STD_LOGIC_VECTOR(63  DOWNTO 0);
            decr_done_o   : OUT    STD_LOGIC
        );
    END COMPONENT;

    SIGNAL trig_i_tb        : STD_LOGIC := '0';
    SIGNAL rst_in_tb        : STD_LOGIC := '0';
    SIGNAL decr_start_in_tb : STD_LOGIC := '0';
    SIGNAL in_ack_tb        : STD_LOGIC := '0';
    SIGNAL data_in_tb       : STD_LOGIC_VECTOR(63 DOWNTO 0) := (OTHERS => '0');
    SIGNAL key_in_tb        : STD_LOGIC_VECTOR(127 DOWNTO 0) := (OTHERS => '0');
    SIGNAL data_o_tb        : STD_LOGIC_VECTOR(63 DOWNTO 0);
    SIGNAL decr_done_o_tb   : STD_LOGIC;

    TYPE test_case_t IS RECORD
        data_in  : STD_LOGIC_VECTOR(63 DOWNTO 0);
        key_in   : STD_LOGIC_VECTOR(127 DOWNTO 0);
        expected_data : STD_LOGIC_VECTOR(63 DOWNTO 0);
    END RECORD;

    TYPE test_case_array IS ARRAY (NATURAL RANGE <>) OF test_case_t;

    CONSTANT test_cases : test_case_array := (
        (x"CF4BBB814C933FDA", x"00000000000000000000000000000000", x"ABCDEF1234565612"),  
        (x"5559B4AC70454796", x"000102030405060708090A0B0C0D0E0F", x"2DA5382F24240AD3"),
        (x"2037EB5D4FF730A4", x"000102030405060708090A0B0C0D0E0F", x"E201F06FEBB34F39"),
        (x"FC8A068B3D17F063", x"00000000000000000000000000000000", x"0123456789ABCDEF"), 
        (x"14F0C75D2BEBD98D", x"000102030405060708090A0B0C0D0E0F", x"0123456789ABCDEF"),
        (x"AC7A5E9FD33632B7", x"0000000089ABCDEF0000000089ABCDEF", x"0123456789ABCDEF")
    );  --encrypted data            key in                          expected


BEGIN
    uut : TEA_decrypt_async_FSM_top
        PORT MAP (
            trig_i        => trig_i_tb,
            rst_in        => rst_in_tb,
            decr_start_in => decr_start_in_tb,
            in_ack        => in_ack_tb,
            data_in       => data_in_tb,
            key_in        => key_in_tb,
            data_o        => data_o_tb,
            decr_done_o   => decr_done_o_tb
        );

        synchro_process : PROCESS
        BEGIN 
            
            -- in_ack_tb <= '0';
            trig_i_tb <= '1';
            WAIT FOR 5 ns;
            -- in_ack_tb <= '1';
            trig_i_tb <= '0';
            WAIT FOR 5 ns;
        END PROCESS;

    stim_proc : PROCESS
    BEGIN
        test_runner_setup(runner, runner_cfg);
        rst_in_tb <= '1';
        WAIT FOR 20 ns;
        rst_in_tb <= '0';
        
        FOR i IN test_cases'RANGE LOOP
            REPORT "Running Test Case " & integer'image(i+1);
            data_in_tb <= test_cases(i).data_in;
            key_in_tb  <= test_cases(i).key_in;
            decr_start_in_tb <= '1';
            WAIT FOR 10 ns;
            decr_start_in_tb <= '0';
            WAIT UNTIL rising_edge(decr_done_o_tb);
            my_is_equal(data_o_tb, test_cases(i).expected_data);
            WAIT FOR 20 ns;
        END LOOP;

        test_runner_cleanup(runner);
    END PROCESS;
END ARCHITECTURE behav;
