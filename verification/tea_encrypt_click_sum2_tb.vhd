LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

use work.tb_pkg.all;
use work.design_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY tea_encrypt_click_sum2_tb IS 
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF tea_encrypt_click_sum2_tb IS

    component tea_encrypt_click_sum IS
        PORT (
            rst_in   : IN  std_logic;
            -- Input channel
            in_req   : in  std_logic;
            in_ack   : out std_logic;
            -- Output channel
            out_req  : out std_logic;
            out_ack  : in  std_logic;
            -----------------------
            cnt_in   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            sum_out  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0):= (OTHERS => '0')
        );
    end component;

    -- Signals
    signal rst_in, in_ack, in_req, out_req, out_ack: std_logic;
    signal sum_out_tb : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal cnt_in  : STD_LOGIC_VECTOR(4 DOWNTO 0)  := (OTHERS => '0');
    
    -- Attributes
    attribute dont_touch : string;
    attribute dont_touch of rst_in, in_ack, in_req, out_req, out_ack : signal is "true";
    attribute dont_touch of sum_out_tb : signal is "true";

BEGIN
    

    info("tea_encrypt_click_sum2_tb");

    -- Process for Stimulus Generation
    stim_proc: process
    begin
        test_runner_setup(runner, runner_cfg);
        -- Initialize signals
        in_req  <= '0';
        cnt_in  <= "00000";
        in_ack  <= '0';
        rst_in  <= '0';

        -- First pulse
        wait for 10 ns;
        in_req <= '1';
        cnt_in <= "00001";

        wait for 10 ns;
        in_req <= '0';
        cnt_in <= "00010";

        -- Second pulse
        wait for 10 ns;
        in_req <= '1';
        cnt_in <= "00011";

        wait for 10 ns;
        in_req <= '0';
        cnt_in <= "00100";

        -- Third pulse
        wait for 10 ns;
        in_req <= '1';
        cnt_in <= "00101";

        -- Delay for acknowledgment
        wait for 5 ns;
        in_ack <= in_req;

        -- Apply Reset
        wait for 350 ns;
        rst_in <= '1';

        -- End Simulation
        test_runner_cleanup(runner);
    end process;

    -- UUT Instantiation
    uut: component tea_encrypt_click_sum
    port map (
        rst_in   => rst_in ,
        in_req   => in_req ,
        in_ack   => in_ack ,
        out_req  => out_req,
        out_ack  => out_ack,
        cnt_in   => cnt_in,        
        sum_out  => sum_out_tb
    );

    
END BEHAV;
