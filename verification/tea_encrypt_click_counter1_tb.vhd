LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

use work.tb_pkg.all;
use work.design_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY tea_encrypt_click_counter1_tb IS
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF tea_encrypt_click_counter1_tb IS

    component tea_encrypt_click_counter is
        port (
            rst_in         : IN  std_logic := '0';
            -- Input channel
            in_req          : in  std_logic := '0';
            in_ack          : out std_logic := '0';
            -- Output channel
            out_req         : out std_logic := '0';
            out_ack         : in  std_logic := '0';
            -----------------------
            cnt_out         : out STD_LOGIC_VECTOR(4 downto 0):= (OTHERS => '0')
        );
    end component;

    signal rst_in, in_ack, in_req, out_req, out_ack: std_logic;
    signal cnt_out : STD_LOGIC_VECTOR(4 downto 0);
    
    attribute dont_touch : string;
    attribute dont_touch of  rst_in, in_ack, in_req, out_req, out_ack : signal is "true";
    attribute dont_touch of   cnt_out : signal is "true";

    BEGIN
    test_runner_setup(runner, runner_cfg);

    info("tea_encrypt_click_counter1_tb");
    
    in_req <= '0'
    , '1' after 50 ns
    , '0' after 100 ns
    , '1' after 150 ns
    , '0' after 200 ns
    , '1' after 250 ns;

    -- my_is_equal(cnt_out,"00010");

    in_ack <= in_req after 5 ns;
    rst_in <= '0', '1' after 600 ns;
    -- my_is_equal(cnt_out,"00000");

    uut : component tea_encrypt_click_counter
    port map (
        rst_in  => rst_in,
        in_req  => in_req,
        in_ack  => in_ack,
        out_req => out_req,
        out_ack => out_ack,
        cnt_out => cnt_out 
    );

    test_runner_cleanup(runner);
END BEHAV;