LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY main_lib;
    use main_lib.tb_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY RC4_1_tb IS
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF RC4_1_tb IS 

CONSTANT N : INTEGER := 256;
CONSTANT key_in_len : integer := 256;

SIGNAL clk_in_tb           : STD_LOGIC := '0';  
SIGNAL rst_in_tb           : STD_LOGIC := '0';
SIGNAL key_in_tb           : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL S_o_tb              : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL key_done_o_tb       : STD_LOGIC := '0';
SIGNAL key_req_o_tb        : STD_LOGIC := '0';
SIGNAL start_in_tb         : STD_LOGIC := '0';
SIGNAL key_unloading_o_tb  : STD_LOGIC := '0';

type key_array is array (0 to 63) of STD_LOGIC_VECTOR(7 DOWNTO 0);

SIGNAL KEY_register : key_array := (
    X"54", X"6F", X"68", X"6C", X"65", X"20", X"6A", X"65", 
    X"20", X"6B", X"72", X"61", X"73", X"6E", X"79", X"20", 
    X"6B", X"6C", X"69", X"63", X"20", X"70", X"73", X"61", 
    X"6E", X"79", X"20", X"76", X"20", X"61", X"73", X"63", 
    X"69", X"69", X"20", X"61", X"20", X"70", X"72", X"65", 
    X"76", X"65", X"64", X"65", X"6E", X"79", X"20", X"64", 
    X"6F", X"20", X"68", X"65", X"78", X"20", X"6A", X"65", 
    X"73", X"74", X"65", X"20", X"74", X"72", X"69", X"2E", others => X"00"
);


type s_array is array (0 to 255) of natural;
constant S_o1 : s_array := (
    216, 196, 32, 206, 198, 82, 153, 144, 
    12, 161, 14, 137, 184, 91, 73, 57, 
    195, 49, 142, 233, 36, 21, 100, 220, 
    96, 20, 94, 151, 147, 102, 186, 121, 
    87, 111, 38, 150, 135, 126, 18, 204, 
    222, 108, 227, 22, 254, 10, 185, 76, 
    235, 59, 11, 68, 42, 13, 56, 120, 
    97, 26, 214, 179, 65, 172, 125, 16, 
    50, 217, 28, 107, 62, 152, 72, 25, 
    90, 29, 129, 103, 148, 211, 48, 124, 
    236, 99, 197, 252, 205, 146, 244, 209, 
    31, 118, 128, 210, 167, 208, 141, 240, 
    47, 234, 2, 183, 155, 58, 127, 249, 
    215, 165, 115, 114, 218, 175, 136, 132, 
    248, 145, 117, 88, 200, 187, 3, 101, 
    139, 229, 168, 212, 95, 15, 251, 134, 
    177, 202, 131, 51, 231, 19, 55, 92, 
    176, 159, 41, 79, 35, 149, 156, 45, 
    43, 178, 160, 69, 119, 0, 170, 213, 
    17, 4, 173, 163, 61, 189, 133, 71, 
    162, 27, 191, 23, 181, 239, 171, 44, 
    1, 104, 67, 143, 116, 242, 109, 77, 
    83, 98, 228, 37, 255, 123, 237, 193, 
    80, 40, 221, 113, 74, 84, 78, 86, 
    158, 225, 253, 180, 238, 203, 6, 201, 
    63, 112, 8, 232, 64, 24, 5, 39, 
    188, 46, 130, 192, 30, 85, 224, 93, 
    122, 81, 199, 219, 157, 247, 190, 66, 
    243, 70, 230, 223, 52, 194, 105, 140, 
    53, 34, 226, 246, 166, 9, 54, 33, 
    164, 75, 169, 106, 182, 207, 110, 7, 
    241, 89, 138, 250, 154, 60, 174, 245
);

    PROCEDURE SEND_KEY(
        SIGNAL key_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        SIGNAL key_req : IN STD_LOGIC
    ) IS
    BEGIN
        for i in 0 TO 63 loop
            -- key_out <= std_logic_vector(to_unsigned(i, 8));
            key_out <= KEY_register(i);
            WAIT UNTIL key_req = '0';
        end loop;
    END SEND_KEY;

BEGIN 

    RC4_KSA_inst: entity work.RC4_KSA
    generic map(
    N => N,
    key_in_len => key_in_len
    )
    port map(
        clk_in     => clk_in_tb,
        rst_in     => rst_in_tb,
        start_in   => start_in_tb,
        key_in     => key_in_tb,
        key_req_o  => key_req_o_tb,
        S_o        => S_o_tb,
        key_done_o => key_done_o_tb,
        key_unloading_o => key_unloading_o_tb
    );

    clk_in_tb <= not clk_in_tb after 10 ns;

    PROCESS IS
    BEGIN
        test_runner_setup(runner, runner_cfg);

        -- Initial reset
        strobe(rst_in_tb, 100 ns);

        strobe(start_in_tb, 10 ns);
        SEND_KEY(key_out => key_in_tb, key_req => key_req_o_tb);
        FOR i IN 0 TO 255 LOOP
            WAIT UNTIL S_o_tb'event;
            check_equal(S_o1(i),S_o_tb);
        END LOOP;
        WAIT FOR 100 ns;
        

        test_runner_cleanup(runner);
    END PROCESS;
END BEHAV;