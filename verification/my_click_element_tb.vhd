LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

use work.tb_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY my_click_element_tb IS
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF my_click_element_tb is
    component my_click_element is
        port (
            rst_in    : in std_logic;
            -- Input channel
            in_ack    : INOUT std_logic;
            in_req    : IN std_logic;
            -- Output channel   
            out_req   : INOUT std_logic;
            out_ack   : in std_logic;
        
            out_fire  : out std_logic
        );
    end component;

    SIGNAL rst_in_tb   : std_logic := '0';
    SIGNAL in_ack_tb   : std_logic := '0';
    SIGNAL in_req_tb   : std_logic := '0';
    SIGNAL out_req_tb  : std_logic := '0';
    SIGNAL out_ack_tb  : std_logic := '0';
    SIGNAL out_fire_tb : std_logic := '0';

    BEGIN

    uut: my_click_element
     port map(
        rst_in   =>  rst_in_tb,
        in_ack   =>  in_ack_tb,
        in_req   =>  in_req_tb,
        out_req  =>  out_req_tb,
        out_ack  =>  out_ack_tb,
        out_fire =>  out_fire_tb
    );

    out_ack_tb <= out_req_tb;

    synchro_process : PROCESS
    BEGIN 
        test_runner_setup(runner, runner_cfg);
        
        -- in_ack_tb <= '0';
        in_req_tb <= '1';
        WAIT FOR 5 ns;
        -- in_ack_tb <= '1';
        in_req_tb <= '0';
        WAIT FOR 5 ns;
    test_runner_cleanup(runner);
    END PROCESS;

    -- stim_proc : PROCESS
    -- BEGIN

    -- END PROCESS;
END ARCHITECTURE behav;