LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE std.env.ALL;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

use std.textio.all;

library main_lib;



PACKAGE tb_pkg IS
  PROCEDURE STOP_SIM;
  PROCEDURE msg(str : string; timestamp : boolean := false; unit : time := ns);
  FUNCTION  vec2string(signal_in: std_logic_vector)RETURN string;
  PROCEDURE my_is_equal(tested_data   : std_logic_vector; expected_data : std_logic_vector; check_name    : string := "");
  PROCEDURE strobe  ( signal sig : out std_logic; constant period_ns : time; constant ACTIVE : STD_LOGIC := '1');
  PROCEDURE UART_WRITE_BYTE ( i_data_in       : in  std_logic_vector(7 downto 0);signal o_serial : out std_logic);
  END PACKAGE tb_pkg;

PACKAGE BODY tb_pkg IS

  PROCEDURE STOP_SIM IS
  BEGIN
    std.env.stop(0);
  END PROCEDURE;

  procedure msg(str : string; timestamp : boolean := false; unit : time := ns) is
    begin
      if timestamp then
        write(output, str & " | time: " & to_string(NOW, unit) & LF);
      else
        write(output, str & LF);
      end if;
    end procedure msg;
  

  function vec2string(signal_in: std_logic_vector) return string is
    variable result : string(1 to signal_in'length); 
  begin
    for i in signal_in'range loop
      if signal_in(i) = '1' then
        result(i+1) := '1';
      else
        result(i+1) := '0';  
      end if;
    end loop;
    return result;
  end function vec2string;

  procedure my_is_equal(tested_data   : std_logic_vector; 
                        expected_data : std_logic_vector;
                        check_name    : string := ""
  ) IS
  BEGIN
    IF (tested_data = expected_data) THEN
    REPORT("------- Passed -------")
    SEVERITY NOTE;
    REPORT("Expected data:"& vec2string(expected_data), " Real data:" & vec2string(tested_data));
  ELSIF (tested_data /= expected_data) THEN
    REPORT("Expected data:"& vec2string(expected_data), " Real data:" & vec2string(tested_data));
    REPORT("------- Failed -------")
    SEVERITY ERROR;
  ELSE
    REPORT("Unexpected state!")
    SEVERITY FAILURE;
  END IF;
  END PROCEDURE my_is_equal;

  procedure strobe (
    signal sig : out std_logic;
    constant period_ns : time;
    constant ACTIVE : std_logic := '1'
  ) is
  begin
    sig <= ACTIVE;
    wait for period_ns;
    sig <= not ACTIVE;
    wait for period_ns;
  end procedure strobe;

  procedure UART_WRITE_BYTE (
    i_data_in       : in  std_logic_vector(7 downto 0);
    signal o_serial : out std_logic
  ) is
  begin

    -- Send Start Bit
    o_serial <= '0';
    wait for 8680 ns;

    -- Send Data Byte
    for ii in 0 to 7 loop
    o_serial <= i_data_in(ii);
    wait for 8680 ns;
    end loop;  -- ii

    -- Send Stop Bit
    o_serial <= '1';
    wait for 8680 ns;
    end UART_WRITE_BYTE;

END PACKAGE BODY tb_pkg;
