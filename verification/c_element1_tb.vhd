LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY c_element1_tb IS
  generic (runner_cfg : string);
END ENTITY c_element1_tb;

ARCHITECTURE behav OF c_element1_tb IS

  -- Component declaration for the Unit Under Test (UUT)
  COMPONENT c_element
  PORT(
    data_in  : IN  std_logic_vector(1 DOWNTO 0);
    data_out : OUT std_logic
  );
  END COMPONENT;

  -- Signals to connect to UUT
  SIGNAL data_in  : std_logic_vector(1 DOWNTO 0);
  SIGNAL data_out : std_logic;

BEGIN

  -- Instantiate the Unit Under Test (UUT)
  uut: c_element
    PORT MAP (
      data_in  => data_in,
      data_out => data_out
    );

  -- Stimulus process
  stim_proc: PROCESS
  BEGIN
  test_runner_setup(runner, runner_cfg);
    
    -- Test case 1: data_in = "00", expect data_out = '0'
    data_in <= "00";
    WAIT FOR 10 ns;
    ASSERT (data_out = '0') REPORT "Test case 1 failed" SEVERITY ERROR;
    ASSERT (data_out = '1') REPORT "Test case 4 passed" SEVERITY NOTE;

    -- Test case 2: data_in = "01", expect no change (no action)
    data_in <= "01";
    WAIT FOR 10 ns;
    ASSERT (data_out = '0') REPORT "Test case 2 failed" SEVERITY ERROR;
    ASSERT (data_out = '1') REPORT "Test case 4 passed" SEVERITY NOTE;

    -- Test case 3: data_in = "10", expect no change (no action)
    data_in <= "10";
    WAIT FOR 10 ns;
    ASSERT (data_out = '0') REPORT "Test case 3 failed" SEVERITY ERROR;
    ASSERT (data_out = '1') REPORT "Test case 4 passed" SEVERITY NOTE;

    -- Test case 4: data_in = "11", expect data_out = '1'
    data_in <= "11";
    WAIT FOR 10 ns;
    ASSERT (data_out = '1') REPORT "Test case 4 failed" SEVERITY ERROR; -- pokud to NEBUDE TAKHLE -> error
    ASSERT (data_out = '0') REPORT "Test case 4 passed" SEVERITY NOTE;

    -- Test case 5: data_in = "10", output should stay at '1
    data_in <= "10";
    WAIT FOR 10 ns;
    ASSERT (data_out = '1') REPORT "Test case 5 failed" SEVERITY ERROR;
    ASSERT (data_out = '0') REPORT "Test case 5 passed" SEVERITY NOTE;

    -- Test case 6: data_in = "01", output should stay at '1
    data_in <= "01";
    WAIT FOR 10 ns;
    ASSERT (data_out = '1') REPORT "Test case 6 failed" SEVERITY ERROR;
    ASSERT (data_out = '0') REPORT "Test case 6 passed" SEVERITY NOTE;

    -- End of simulation
    -- WAIT;
    test_runner_cleanup(runner);
  END PROCESS;

END ARCHITECTURE behav;