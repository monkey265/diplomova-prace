LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY TEA_encrypt_click_init1_tb IS
    generic (runner_cfg : string);
END ENTITY TEA_encrypt_click_init1_tb;

ARCHITECTURE behav OF TEA_encrypt_click_init1_tb IS

    component TEA_encrypt_click_init is
        port (
            rst_in         : IN std_logic;
            in_req         : IN std_logic;
            in_ack         : OUT std_logic;
            out_req        : OUT std_logic;
            out_ack        : IN std_logic;
            data_in        : IN std_logic_vector(63 DOWNTO 0);
            key_in         : IN std_logic_vector(127 DOWNTO 0);
            v0             : OUT std_logic_vector(31 DOWNTO 0);
            v1             : OUT std_logic_vector(31 DOWNTO 0);
            k0, k1, k2, k3 : OUT std_logic_vector(31 DOWNTO 0)
            
        );
    end component;

    SIGNAL rst_in_tb         : std_logic;
    SIGNAL in_req_tb         : std_logic;
    SIGNAL in_ack_tb         : std_logic;
    SIGNAL out_req_tb        : std_logic;
    SIGNAL out_ack_tb        : std_logic;
    SIGNAL data_in_tb        : std_logic_vector(63 DOWNTO 0);
    SIGNAL key_in_tb         : std_logic_vector(127 DOWNTO 0);
    SIGNAL v0_tb             : std_logic_vector(31 DOWNTO 0);
    SIGNAL v1_tb             : std_logic_vector(31 DOWNTO 0);
    SIGNAL k0_tb             : std_logic_vector(31 DOWNTO 0);
    SIGNAL k1_tb             : std_logic_vector(31 DOWNTO 0);
    SIGNAL k2_tb             : std_logic_vector(31 DOWNTO 0);
    SIGNAL k3_tb             : std_logic_vector(31 DOWNTO 0);

BEGIN

uut : TEA_encrypt_click_init
    PORT MAP(
        rst_in   => rst_in_tb,        
        in_req   => in_req_tb,        
        in_ack   => in_ack_tb,        
        out_req  => out_req_tb,        
        out_ack  => out_ack_tb,        
        data_in  => data_in_tb,        
        key_in   => key_in_tb,        
        v0       => v0_tb,        
        v1       => v1_tb,        
        k0       => k0_tb,
        k1       => k1_tb,
        k2       => k2_tb,
        k3       => k3_tb   
    ); 

        -- Test process
    stim_proc : PROCESS
    BEGIN
        test_runner_setup(runner, runner_cfg);
        WAIT FOR 1 ns;
        key_in_tb  <= X"000102030405060708090A0B0C0D0E0F";
        data_in_tb <= X"0123456789ABCDEF";

        WAIT FOR 20 ns;
        check_equal(k0_tb,key_in_tb(127 DOWNTO 96),"K0");
        check_equal(k1_tb,key_in_tb(95  DOWNTO 64),"K1");
        check_equal(k2_tb,key_in_tb(63  DOWNTO 32),"K2");
        check_equal(k3_tb,key_in_tb(31  DOWNTO 0), "K3");

        -- V0 and V1 check
        check_equal(v0_tb,data_in_tb(63 DOWNTO 32),"V0");
        check_equal(v1_tb,data_in_tb(31 DOWNTO  0),"V1");
        

    test_runner_cleanup(runner);
    END PROCESS;

END ARCHITECTURE behav;
