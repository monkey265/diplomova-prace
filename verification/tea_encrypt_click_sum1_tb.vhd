LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

use work.tb_pkg.all;
use work.design_pkg.all;

LIBRARY VUNIT_LIB;
CONTEXT VUNIT_LIB.VUNIT_CONTEXT;

ENTITY tea_encrypt_click_sum1_tb IS 
    generic (runner_cfg : string);
END ENTITY;

ARCHITECTURE behav OF tea_encrypt_click_sum1_tb IS

    component tea_encrypt_click_sum IS
        PORT (
            rst_in          : IN  std_logic;
            -- Input channel
            in_req          : in  std_logic;
            in_ack          : out std_logic;
            -- Output channel
            out_req         : out std_logic;
            out_ack         : in  std_logic;
            -----------------------
            cnt_in          : IN STD_LOGIC_VECTOR(4 DOWNTO 0):= (OTHERS => '0');
            sum_out         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0')
        );
    end component tea_encrypt_click_sum;

    signal rst_in, in_ack, in_req, out_req, out_ack: std_logic;
    signal sum_out : STD_LOGIC_VECTOR(31 downto 0):= (OTHERS => '0');
    signal cnt_in  : STD_LOGIC_VECTOR(4 DOWNTO 0);
    
    attribute dont_touch : string;
    attribute dont_touch of  rst_in, in_ack, in_req, out_req, out_ack : signal is "true";
    attribute dont_touch of  sum_out : signal is "true";

    BEGIN
    test_runner_setup(runner, runner_cfg);

    info("tea_encrypt_click_sum1_tb");
    
    in_req <= '0'
    , '1' after 50 ns
    , '0' after 100 ns
    , '1' after 150 ns
    , '0' after 200 ns
    , '1' after 250 ns;

    cnt_in <= "00000"
    , "00001" after 50 ns
    , "00010" after 100 ns
    , "00011" after 150 ns
    , "00100" after 200 ns
    , "00101" after 250 ns;

    in_ack <= in_req after 5 ns;
    rst_in <= '0', '1' after 600 ns;

    uut: component tea_encrypt_click_sum
    port map (
        rst_in   => rst_in ,
        in_req   => in_req ,
        in_ack   => in_ack ,
        out_req  => out_req,
        out_ack  => out_ack,
        cnt_in   => cnt_in,        
        sum_out  => sum_out
    );

    test_runner_cleanup(runner);
    END BEHAV;