#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (c) 2014-2025, Lars Asplund lars.anders.asplund@gmail.com

from pathlib import Path
from vunit import VUnit
from subprocess import call


def post_run(results):
    results.merge_coverage(file_name="coverage_data")
    if VU.get_simulator_name() == "ghdl":
        if results._simulator_if._backend == "gcc":
            call(["gcovr", "coverage_data"])
        else:
            call(["gcovr", "-a", "coverage_data/gcovr.json"])


VU = VUnit.from_argv()
VU.add_vhdl_builtins()
VU.add_external_library("unisim", "vivado_libs/unisim")

main_lib = VU.add_library("main_lib")
fpga_lib = VU.add_library("fpga_lib")

# Adding libraries
main_lib.add_source_files("source/*.vhd")
main_lib.add_source_files("source/components/*.vhd")
main_lib.add_source_files("verification/*.vhd")

fpga_lib.add_source_files("fpga/sources/*.vhd")
fpga_lib.add_source_files("fpga/components/*.vhd")
fpga_lib.add_source_files("fpga/tb/*.vhd")

main_lib.set_sim_option("enable_coverage", True)
fpga_lib.set_sim_option("enable_coverage", True)


main_lib.set_compile_option("modelsim.vcom_flags", ["+cover=bs"])
main_lib.set_compile_option("modelsim.vlog_flags", ["+cover=bs"])
main_lib.set_compile_option("enable_coverage", True)

fpga_lib.set_compile_option("modelsim.vcom_flags", ["+cover=bs"])
fpga_lib.set_compile_option("modelsim.vlog_flags", ["+cover=bs"])
fpga_lib.set_compile_option("enable_coverage", True)

VU.main(post_run=post_run)