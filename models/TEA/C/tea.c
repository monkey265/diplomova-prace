#include <stdio.h>
#include <stdint.h>

void encrypt (uint32_t v[2], const uint32_t k[4]) {
    uint32_t v0 = v[0], v1 = v[1], sum = 0, i;
    uint32_t delta = 0x9E3779B9;
    uint32_t k0 = k[0], k1 = k[1], k2 = k[2], k3 = k[3];
    for (i = 0; i < 32; i++) {
        printf("sum at start: %08X\n", sum);
        sum += delta;
        v0 += ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
        v1 += ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
        printf("After round %d (encryption): v0 = %08X, v1 = %08X, sum = %08X\n", i + 1, v0, v1, sum);
        // printf("After round %d (encryption): sum = %08X\n", i + 1, sum);
        // printf("After round %d (encryption): delta = %08X\n", i + 1, delta);
    }
    v[0] = v0; v[1] = v1;
}

void decrypt (uint32_t v[2], const uint32_t k[4]) {
    uint32_t v0 = v[0], v1 = v[1], sum = 0xC6EF3720, i;
    uint32_t delta = 0x9E3779B9;
    uint32_t k0 = k[0], k1 = k[1], k2 = k[2], k3 = k[3];
    for (i = 0; i < 32; i++) {
        v1 -= ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
        v0 -= ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
        sum -= delta;
        printf("After round %d (decryption): v0 = %08X, v1 = %08X\n", i + 1, v0, v1);
    }
    v[0] = v0; v[1] = v1;
}

int main() {           
    uint32_t v[2] = {0xABCDEF12, 0x34565612}; // Původní data (plaintext)
    uint32_t k[4] = {0x00000000, 0x00000000, 0x00000000, 0x00000000}; // Klíč

    printf("Original data: v0 = %08X, v1 = %08X\n", v[0], v[1]);
    printf("Key: k0 = %08X, k1 = %08X, k2 = %08X, k3 = %08X\n", k[0], k[1], k[2], k[3]);
    encrypt(v, k); // Šifrování
    printf("Encrypted data: v0 = %08X, v1 = %08X\n", v[0], v[1]);

    decrypt(v, k); // Dešifrování
    printf("Decrypted data: v0 = %08X, v1 = %08X\n", v[0], v[1]);

    return 0;
}
