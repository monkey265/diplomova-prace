def encrypt(v, k):
    """
    Encrypts a 64-bit block using the TEA (Tiny Encryption Algorithm).

    Args:
    v (list of int): A list with two 32-bit integers [v0, v1] representing the block to encrypt.
    k (list of int): A list with four 32-bit integers [k0, k1, k2, k3] representing the key.

    Returns:
    list of int: The encrypted 64-bit block as a list [v0, v1].
    """
    v0, v1 = v[0], v[1]
    sum = 0
    delta = 0x9E3779B9
    k0, k1, k2, k3 = k[0], k[1], k[2], k[3]

    for _ in range(32):
        sum = (sum + delta) & 0xFFFFFFFF
        v0 = (v0 + (((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1))) & 0xFFFFFFFF
        v1 = (v1 + (((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3))) & 0xFFFFFFFF

    return [v0, v1]
