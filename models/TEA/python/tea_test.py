from mytea import *

data_to_encrypt = 0x0000000000000000
key = 0x00000000000000000000000000000000

# Convert data and key to lists of 32-bit integers

def int_to_list(value, num_parts):
    parts = []
    mask = 0xFFFFFFFF

    for _ in range(num_parts):
      parts.insert(0,value & mask)
      value >>=32
    return parts

data_list = int_to_list(data_to_encrypt, 2)
key_list = int_to_list(key, 4)

print(f"Original data: {data_list}")
print(f"Key: {key_list}")

encrypted_data = encrypt(data_list, key_list)

# Correctly print the encrypted data
print(f"Encrypted data: {[hex(val) for val in encrypted_data]}")  # Use list comprehension

# Or, if you want to combine it into a single hex string:
encrypted_hex = "".join([hex(val)[2:].zfill(8) for val in encrypted_data]) #remove 0x, add leading 0 if needed
print(f"Encrypted data (combined): 0x{encrypted_hex}")


