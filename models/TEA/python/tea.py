from pytea import TEA

# Key must be a 16-byte byte string
# key = 0x000102030405060708090A0B0C0D0E0F
key = 0x00000000000000000000000000000000   
key_bytes = key.to_bytes(16, byteorder='big')

# Initialize the TEA cipher
tea = TEA(key_bytes)

# Data to be encrypted (must be an 8-byte byte string)
data_in1 = 0x0000000000000000
print('Data_in1  (hex):', hex(data_in1))
data_in1_bytes = data_in1.to_bytes(8, byteorder='big')

# Encryption
data_in1_encrypted = tea.encrypt(data_in1_bytes)
print('Key:', key_bytes)
print('Data_in1 encrypted (hex):', data_in1_encrypted.hex())

# Decryption
data_in1_decrypted = tea.decrypt(data_in1_encrypted)

# Convert decrypted bytes back to integer for comparison
data_in1_recovered = int.from_bytes(data_in1_decrypted, byteorder='big')
print('Data_in1 decrypted (integer):', data_in1_recovered)
print('Data_in1 decrypted (hex):', hex(data_in1_recovered))


# Verification 
if data_in1_recovered == data_in1:
    print("Decryption successful!")
else:
    print("Decryption failed!")

