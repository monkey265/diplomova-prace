#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 256   // 2^8

// Function to swap two unsigned characters.
void swap(unsigned char *a, unsigned char *b) {
    unsigned char tmp = *a;
    *a = *b;
    *b = tmp;
}

int KSA(char *key, unsigned char *S) {
    int len = strlen(key);
    int j = 0;
    printf("KSA: Initializing S array...\n");
    printf("Key length is: %d \n", len);
    for(int i = 0; i < N; i++) {
        S[i] = i;
    }
    
    printf("KSA: Key Scheduling...\n");
    for(int i = 0; i < N; i++) {
        printf("KSA: Starting iteration i=%d, current j=%d\n", i, j);
        
        // Use the ASCII value of the character directly, just like before
        int key_byte = key[i % len];
        printf("KSA: Using key byte at position %d (i %% len = %d): key[%d] = %d (ASCII '%c')\n", 
               i % len, i % len, i % len, key_byte, key_byte);
        
        int j_prev = j;
        j = j + S[i] + key_byte;
        printf("KSA: j calculati+on: j_prev(%d) + S[i](%d) + key_byte(%d) = %d\n", j_prev, S[i], key_byte, j);
        
        j = j % N;
        printf("KSA: j after modulo N(%d): %d\n", N, j);
        
        printf("KSA: Swapping S[%d] and S[%d]\n", i, j);
        swap(&S[i], &S[j]);
        printf("KSA: S[%d] = %d, S[%d] = %d\n", i, S[i], j, S[j]);
        
        printf("KSA: End of iteration i=%d\n", i);
        printf("----------------------------------------\n");
    }
    
    printf("KSA: Key Scheduling Complete.\n");
    return 0;
}

int main(int argc, char* argv[]){
    char *key; // Declare key here
    if (argc != 2) {
        // If no command-line argument, use a default key
        // key = "000102030405060708090A0B0C0D0E0F000102030405060708090A0B0C0D0E0F"; // Default key
        key = "Tohle je krasny klic psany v ascii a prevedeny do hex jeste tri.";
        printf("Main: No key provided as argument. Using default key: %s\n", key);
    } else {
        // If a command-line argument is provided, use it as the key
        key = argv[1];
        printf("Main: Key provided: %s\n", key);
    }
    
    unsigned char S[N];
    
    KSA(key, S);
    printf("Main: S array after KSA:\n");
    for(int i=0; i<N; ++i){
        printf("Main: S[%d] = %d\n", i, S[i]);
    }
    return 0;
}
