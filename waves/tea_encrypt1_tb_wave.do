onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tea_encrypt1_tb/clk_in_tb
add wave -noupdate /tea_encrypt1_tb/rst_in_tb
add wave -noupdate /tea_encrypt1_tb/data_in_tb
add wave -noupdate /tea_encrypt1_tb/key_in_tb
add wave -noupdate /tea_encrypt1_tb/data_o_tb
add wave -noupdate /tea_encrypt1_tb/encr_done_o_tb
add wave -noupdate /tea_encrypt1_tb/encr_start_in_tb
add wave -noupdate /tea_encrypt1_tb/uut/k0
add wave -noupdate /tea_encrypt1_tb/uut/k1
add wave -noupdate /tea_encrypt1_tb/uut/k2
add wave -noupdate /tea_encrypt1_tb/uut/k3
add wave -noupdate /tea_encrypt1_tb/uut/sum
add wave -noupdate /tea_encrypt1_tb/uut/v1
add wave -noupdate /tea_encrypt1_tb/uut/v0
add wave -noupdate /tea_encrypt1_tb/uut/state
add wave -noupdate /tea_encrypt1_tb/uut/next_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {4089750 ps}
run -all
