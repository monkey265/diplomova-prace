onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tea_combined_tb/clk_in_tb
add wave -noupdate -radix hexadecimal /tea_combined_tb/data_decrypted_tb
add wave -noupdate -radix hexadecimal /tea_combined_tb/data_encrypted_tb
add wave -noupdate -radix hexadecimal /tea_combined_tb/data_in_tb
add wave -noupdate /tea_combined_tb/decr_done_o_tb
add wave -noupdate /tea_combined_tb/decr_start_in_tb
add wave -noupdate /tea_combined_tb/encr_done_o_tb
add wave -noupdate /tea_combined_tb/encr_start_in_tb
add wave -noupdate /tea_combined_tb/key_in_tb
add wave -noupdate /tea_combined_tb/rst_in_tb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {432260 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {3102750 ps}
run -all
