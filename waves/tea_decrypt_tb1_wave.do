onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tea_decrypt_tb1/clk_in_tb
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/data_in_tb
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/data_o_tb
add wave -noupdate /tea_decrypt_tb1/decr_done_o_tb
add wave -noupdate /tea_decrypt_tb1/decr_start_in_tb
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/key_in_tb
add wave -noupdate /tea_decrypt_tb1/rst_in_tb
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/k0
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/k1
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/k2
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/k3
add wave -noupdate -radix unsigned -radixshowbase 0 /tea_decrypt_tb1/uut/counter_int
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/sum
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/v0
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tea_decrypt_tb1/uut/v1
add wave -noupdate /tea_decrypt_tb1/uut/state
add wave -noupdate /tea_decrypt_tb1/uut/next_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {170742 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {1034250 ps}
run -all
