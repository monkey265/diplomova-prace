onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uart_controller1_tb/clk_in_tb
add wave -noupdate /uart_controller1_tb/rst_in_tb
add wave -noupdate /uart_controller1_tb/rx_data_in_tb
add wave -noupdate /uart_controller1_tb/uart_controller_inst/rx_data_reg
add wave -noupdate /uart_controller1_tb/tx_data_in_tb
add wave -noupdate /uart_controller1_tb/rx_data_ready_in_tb
add wave -noupdate /uart_controller1_tb/tx_requested_in_tb
add wave -noupdate /uart_controller1_tb/tx_ready_in_tb
add wave -noupdate /uart_controller1_tb/encr_requested_o_tb
add wave -noupdate /uart_controller1_tb/tx_data_o_tb
add wave -noupdate -color Salmon /uart_controller1_tb/uart_controller_inst/state
add wave -noupdate /uart_controller1_tb/uart_controller_inst/next_state
add wave -noupdate /uart_controller1_tb/uart_controller_inst/decoding_err_reg
add wave -noupdate /uart_controller1_tb/err_flag_o_tb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {150446 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {861 ns}
run -all
