onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tea_encrypt_click_counter1_tb/in_req
add wave -noupdate -radix binary -radixshowbase 0 /tea_encrypt_click_counter1_tb/cnt_out
add wave -noupdate -radix binary -radixshowbase 0 /tea_encrypt_click_counter1_tb/uut/cnt_internal
add wave -noupdate /tea_encrypt_click_counter1_tb/rst_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {177769 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {630 ns}
run -all
