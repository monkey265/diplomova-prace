onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {UART Ctrl}
add wave -noupdate /uart_controller3_tb/clk_in_tb
add wave -noupdate /uart_controller3_tb/rst_in_tb
add wave -noupdate /uart_controller3_tb/rx_data_in_tb
add wave -noupdate /uart_controller3_tb/tx_data_in_tb
add wave -noupdate /uart_controller3_tb/rx_data_ready_in_tb
add wave -noupdate /uart_controller3_tb/tx_requested_in_tb
add wave -noupdate /uart_controller3_tb/tx_ready_in_tb
add wave -noupdate /uart_controller3_tb/encr_requested_o_tb
add wave -noupdate /uart_controller3_tb/err_flag_o_tb
add wave -noupdate /uart_controller3_tb/tx_active_o_tb
add wave -noupdate /uart_controller3_tb/tx_data_o_tb
add wave -noupdate /uart_controller3_tb/o_rx_byte_tb
add wave -noupdate /uart_controller3_tb/tx_done_tb
add wave -noupdate -divider UART_RX
add wave -noupdate /uart_controller3_tb/UART_RX_inst/i_RX_Serial
add wave -noupdate /uart_controller3_tb/UART_RX_inst/o_RX_DV
add wave -noupdate /uart_controller3_tb/UART_RX_inst/o_RX_Byte
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_SM_Main
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_RX_Data_R
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_RX_Data
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_Clk_Count
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_Bit_Index
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_RX_Byte
add wave -noupdate /uart_controller3_tb/UART_RX_inst/r_RX_DV
add wave -noupdate -divider UART_TX
add wave -noupdate /uart_controller3_tb/UART_TX_inst/i_Clk
add wave -noupdate /uart_controller3_tb/UART_TX_inst/i_TX_DV
add wave -noupdate /uart_controller3_tb/UART_TX_inst/i_TX_Byte
add wave -noupdate /uart_controller3_tb/UART_TX_inst/o_TX_Active
add wave -noupdate /uart_controller3_tb/UART_TX_inst/o_TX_Serial
add wave -noupdate /uart_controller3_tb/UART_TX_inst/o_TX_Done
add wave -noupdate /uart_controller3_tb/UART_TX_inst/r_SM_Main
add wave -noupdate /uart_controller3_tb/UART_TX_inst/r_Clk_Count
add wave -noupdate /uart_controller3_tb/UART_TX_inst/r_Bit_Index
add wave -noupdate /uart_controller3_tb/UART_TX_inst/r_TX_Data
add wave -noupdate /uart_controller3_tb/UART_TX_inst/r_TX_Done
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17830000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {37012500 ps}
run -all
