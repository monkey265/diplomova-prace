onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/N
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/clk_in
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/rst_in
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/key_start_in
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/plaintext_load
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/key_in
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/plaintext_in
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/state
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/next_state
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/S_reg
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/P_reg
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/i
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/j
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/key_cntr
add wave -noupdate /rc4_2_tb/RC4_PRGA_inst/plaintxt_cntr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1340913 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {10710 ns}
