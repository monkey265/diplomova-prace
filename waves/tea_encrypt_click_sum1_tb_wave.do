onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tea_encrypt_click_sum1_tb/rst_in
add wave -noupdate /tea_encrypt_click_sum1_tb/in_req
add wave -noupdate /tea_encrypt_click_sum1_tb/sum_out
add wave -noupdate /tea_encrypt_click_sum1_tb/cnt_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {193760 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {19638 ps} {452892 ps}
run -all
