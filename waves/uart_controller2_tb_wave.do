onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {UART RX}
add wave -noupdate /uart_controller2_tb/UART_RX_INST/i_Clk
add wave -noupdate /uart_controller2_tb/UART_RX_INST/i_RX_Serial
add wave -noupdate /uart_controller2_tb/UART_RX_INST/o_RX_DV
add wave -noupdate /uart_controller2_tb/UART_RX_INST/o_RX_Byte
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_SM_Main
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_RX_Data_R
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_RX_Data
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_Clk_Count
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_Bit_Index
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_RX_Byte
add wave -noupdate /uart_controller2_tb/UART_RX_INST/r_RX_DV
add wave -noupdate -divider {UART TX}
add wave -noupdate /uart_controller2_tb/UART_TX_INST/g_CLKS_PER_BIT
add wave -noupdate /uart_controller2_tb/UART_TX_INST/i_TX_DV
add wave -noupdate /uart_controller2_tb/UART_TX_INST/i_TX_Byte
add wave -noupdate /uart_controller2_tb/UART_TX_INST/o_TX_Active
add wave -noupdate /uart_controller2_tb/UART_TX_INST/o_TX_Serial
add wave -noupdate /uart_controller2_tb/UART_TX_INST/o_TX_Done
add wave -noupdate /uart_controller2_tb/UART_TX_INST/r_SM_Main
add wave -noupdate /uart_controller2_tb/UART_TX_INST/r_Clk_Count
add wave -noupdate /uart_controller2_tb/UART_TX_INST/r_Bit_Index
add wave -noupdate /uart_controller2_tb/UART_TX_INST/r_TX_Data
add wave -noupdate /uart_controller2_tb/UART_TX_INST/r_TX_Done
add wave -noupdate -divider {UART CTRL}
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/clk_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/rst_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/rx_data_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/tx_data_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/rx_data_ready_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/tx_requested_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/tx_ready_in
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/encr_requested_o
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/err_flag_o
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/tx_data_o
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/rx_data_reg
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/encr_requested_reg
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/decoding_err_reg
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/tx_ready_reg
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/tx_data_reg
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/state
add wave -noupdate /uart_controller2_tb/UART_CTRL_INST/next_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {120751982 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 196
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {323452500 ps}
run -all
