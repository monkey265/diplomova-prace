onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /my_click_element_tb/rst_in_tb
add wave -noupdate /my_click_element_tb/in_ack_tb
add wave -noupdate /my_click_element_tb/in_req_tb
add wave -noupdate /my_click_element_tb/out_req_tb
add wave -noupdate /my_click_element_tb/out_ack_tb
add wave -noupdate /my_click_element_tb/out_fire_tb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10066 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {21 ns}
run -all
