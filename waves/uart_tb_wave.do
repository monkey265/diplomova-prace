onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider UART_TX
add wave -noupdate -radix decimal /uart_tb/UART_TX_INST/g_CLKS_PER_BIT
add wave -noupdate /uart_tb/UART_TX_INST/i_TX_Byte
add wave -noupdate /uart_tb/UART_TX_INST/o_TX_Active
add wave -noupdate /uart_tb/UART_TX_INST/r_SM_Main
add wave -noupdate -radix decimal -radixshowbase 0 /uart_tb/UART_TX_INST/r_Clk_Count
add wave -noupdate -radix binary /uart_tb/UART_TX_INST/r_Bit_Index
add wave -noupdate /uart_tb/UART_TX_INST/r_TX_Done
add wave -noupdate -divider UART_RX
add wave -noupdate -radix decimal /uart_tb/UART_RX_INST/g_CLKS_PER_BIT
add wave -noupdate -radix binary /uart_tb/UART_RX_INST/o_RX_Byte
add wave -noupdate /uart_tb/UART_RX_INST/r_SM_Main
add wave -noupdate /uart_tb/UART_RX_INST/r_RX_Data_R
add wave -noupdate /uart_tb/UART_RX_INST/r_RX_Data
add wave -noupdate -radix decimal -radixshowbase 0 /uart_tb/UART_RX_INST/r_Clk_Count
add wave -noupdate /uart_tb/UART_RX_INST/r_Bit_Index
add wave -noupdate /uart_tb/UART_RX_INST/r_RX_Byte
add wave -noupdate /uart_tb/UART_RX_INST/o_RX_DV
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {170450000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {166410158 ps} {174242962 ps}
run -all
