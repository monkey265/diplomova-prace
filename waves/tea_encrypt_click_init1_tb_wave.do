onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tea_encrypt_click_init1_tb/uut/data_in
add wave -noupdate /tea_encrypt_click_init1_tb/uut/key_in
add wave -noupdate /tea_encrypt_click_init1_tb/uut/v0
add wave -noupdate /tea_encrypt_click_init1_tb/uut/v1
add wave -noupdate /tea_encrypt_click_init1_tb/uut/k0
add wave -noupdate /tea_encrypt_click_init1_tb/uut/k1
add wave -noupdate /tea_encrypt_click_init1_tb/uut/k2
add wave -noupdate /tea_encrypt_click_init1_tb/uut/k3
add wave -noupdate /tea_encrypt_click_init1_tb/in_req_tb
add wave -noupdate /tea_encrypt_click_init1_tb/uut/delay_req/s_connect
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7318 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {32191 ps}
run -all
